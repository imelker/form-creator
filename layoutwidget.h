#ifndef LAYOUTWIDGET_H
#define LAYOUTWIDGET_H

#include <QWidget>
#include <QMap>
#include <QLayout>
#include <QDrag>
#include <QDragEnterEvent>
#include <QMimeData>
#include "enums.h"
#include "label.h"
#include "pushbutton.h"
#include "checkbox.h"

class LayoutWidget : public QWidget
{
    Q_OBJECT
    static int countID(const int flag, const int id) //flag: 0 - обычная работа, 1 вызов при delete
    {
        static QVector<int> _ids;
        if(flag == 0){
            if(_ids.isEmpty())
                _ids.append(_ids.size());
            else if (_ids.last()!=-1)
                _ids.append(_ids.size());

            for(int i = 0; i < _ids.size();i++)
            {
                if(_ids.at(i)==-1){
                    _ids.removeAt(i);
                    _ids.insert(i,i);
                    qDebug() << _ids.at(i)<< " - " << i;
                    return i;
                } else if (i==_ids.size()-1)
                {
                    return i;
                }
            }
        } else if (1)
        {
            _ids.removeAt(id);
            _ids.insert(id,-1);
            return -1;
        }
    }
public:
    explicit LayoutWidget(WidgetType newType, QWidget *parent = 0);
    ~LayoutWidget();
    QMenu* contextMenu;

private:
    QMap<QString, QWidget*>* children;
    QTreeWidgetItem* insepctorItem;
    QLayout* thisLayout;

    bool _selected;
    int thisID;
    bool drag;
    int mousePosX,mousePosY;
    ResizeType resizeType;
    QRect _lt,_rt,_mt,_lb,_rb,_mb,_lm,_rm;
    QRect tempRect;
    int resiseRectSize;
    void resizeWidget(QMouseEvent *event);
    void setFixedS(bool fix);

public:
    WidgetType type;
    bool hasChildren();
    bool clearChildren();
    void insertToLayout(QString key, QWidget* wid);
    bool isSelected(){return _selected;}
    QMap<QString, QWidget*>* layoutChildren(){return children;}
    void setSelected(bool value);
    void setTreeItem(QTreeWidgetItem* item);
    QWidget* takeWidget(QString key);
    QWidget* insertWidget(QWidget *widget);
    QTreeWidgetItem* getTreeItem(){return insepctorItem;}


signals:
    void selected(QWidget*);
    void addingToLayout(QWidget* parentWidget, WidgetType widgetType, QPoint pos  = QPoint());
    void draggingToForm(QWidget* parentWidget, WidgetType widgetType, QPoint pos, QWidget* dragedWidget);

protected: //переопределение виртуальных методов
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void mouseReleaseEvent(QMouseEvent* /*event*/);
    void mouseMoveEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
};

#endif // LAYOUTWIDGET_H
