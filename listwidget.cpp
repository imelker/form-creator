#include "listwidget.h"

#include <QDrag>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QPixmap>
#include "layoutwidget.h"
#include "label.h"
#include "pushbutton.h"
#include "checkbox.h"

ListWidget::ListWidget(QWidget *parent) : QListWidget(parent)
{
    //настройки ListWidget'a
    setDragEnabled(true);
    setDropIndicatorShown(true);
    setIconSize(QSize(20,20));
}

bool ListWidget::hasRightFormat(QDragEnterEvent *enterEvent, QDragMoveEvent *moveEvent, QDropEvent *dropEvent) //проверка нужный ли файл был взят
{
    if(enterEvent!=NULL)
        return enterEvent->mimeData()->hasFormat("label")
                || enterEvent->mimeData()->hasFormat("vlayout")
                || enterEvent->mimeData()->hasFormat("hlayout")
                || enterEvent->mimeData()->hasFormat("button")
                || enterEvent->mimeData()->hasFormat("checkbox");
    else if(moveEvent!=NULL)
        return enterEvent->mimeData()->hasFormat("label")
                || enterEvent->mimeData()->hasFormat("vlayout")
                || enterEvent->mimeData()->hasFormat("hlayout")
                || enterEvent->mimeData()->hasFormat("button")
                || enterEvent->mimeData()->hasFormat("checkbox");
    else if(dropEvent!=NULL)
        return enterEvent->mimeData()->hasFormat("label")
                || enterEvent->mimeData()->hasFormat("vlayout")
                || enterEvent->mimeData()->hasFormat("hlayout")
                || enterEvent->mimeData()->hasFormat("button")
                || enterEvent->mimeData()->hasFormat("checkbox");
    return false;
}

void ListWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (hasRightFormat(event,NULL,NULL))
        event->accept();
    else
        event->ignore();
}

void ListWidget::dragMoveEvent(QDragMoveEvent *event)
{
    if (hasRightFormat(NULL,event,NULL))
    {
            event->setDropAction(Qt::MoveAction);
            event->accept();
    }
        else
            event->ignore();
}

void ListWidget::dropEvent(QDropEvent *event)
{

    if (hasRightFormat(NULL,NULL,event))
        event->accept();
    else
        event->ignore();
}

void ListWidget::startDrag(Qt::DropActions supportedActions)
{
    QListWidgetItem *item = currentItem();

    QByteArray itemData = NULL;
    QPixmap* pixmap;
    QMimeData *mimeData = new QMimeData;
    QDrag *drag = new QDrag(this);
    switch (currentRow())
    {
    case 1:
    {
        LayoutWidget* widget = new LayoutWidget(WidgetType::VBoxLayout,this->parentWidget());
        pixmap = new QPixmap(widget->size());
        widget->render(pixmap);
        delete widget;
        mimeData->setData("vlayout" , itemData);
        break;
    }
    case 2:
    {
        LayoutWidget* widget = new LayoutWidget(WidgetType::HBoxLayout,this->parentWidget());
        pixmap = new QPixmap(widget->size());
        widget->render(pixmap);
        delete widget;
        mimeData->setData("hlayout" , itemData);
        break;
    }
    case 7:
    {
        Label* widget = new Label(this->parentWidget());
        pixmap = new QPixmap(widget->size());
        widget->render(pixmap);
        delete widget;
        mimeData->setData("label" , itemData);
        break;
    }
    case 8:
    {
        PushButton* widget = new PushButton(this->parentWidget());
        pixmap = new QPixmap(widget->size());
        widget->render(pixmap);
        delete widget;
        mimeData->setData("button", itemData);
        break;
    }
    case 9:
    {
        CheckBox* widget = new CheckBox(this->parentWidget());
        pixmap = new QPixmap(widget->size());
        widget->render(pixmap);
        delete widget;
        mimeData->setData("checkbox", itemData);
        break;
    }
    default:
        return;
        break;
    }

    drag->setMimeData(mimeData);
    drag->setHotSpot(QPoint( pixmap->width()/2, pixmap->height()/2));
    drag->setPixmap(*pixmap);

    if (drag->exec(Qt::MoveAction) == Qt::MoveAction)
    {
        true;
    }

}

