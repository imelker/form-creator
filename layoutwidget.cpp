#include "layoutwidget.h"
#include <QMenu>

LayoutWidget::LayoutWidget(WidgetType newType,QWidget *parent) : QWidget(parent)
{
    thisID = countID(0,-1);
    if(thisID!=0)
        setObjectName("layout_" + QString::number(thisID));
    else
        setObjectName("layout");

    //настройки LayoutWidget'a
    children = new QMap<QString, QWidget*>();
    resizeType = ResizeType::Null;
    resiseRectSize = 5;
    _selected = false;
    drag = false;
    this->setMinimumSize(QSize(20,20));
    setAcceptDrops(true);
    contextMenu = new QMenu(this);

    type = newType;
    switch (type) {
    case WidgetType::Null:
    {
        thisLayout = NULL;
        break;
    }
    case WidgetType::HBoxLayout:
    {
        thisLayout = new QHBoxLayout(this);
        this->setLayout(thisLayout);
        break;
    }
    case WidgetType::VBoxLayout:
    {
        thisLayout = new QVBoxLayout(this);
        this->setLayout(thisLayout);
        break;
    }
    default:
        break;
    }

    this->layout()->setMargin(3);
    this->layout()->setSpacing(3);
    this->layout()->setAlignment(Qt::AlignCenter);
}

LayoutWidget::~LayoutWidget()
{
    countID(1,thisID);
}

bool LayoutWidget::hasChildren()
{
    if(children->size()!=0)
        return true;
    else
        return false;
}

bool LayoutWidget::clearChildren()
{
    for(int i=0;i<children->size();i++){
        if(dynamic_cast<Label*>(children->values().at(i))!=NULL)
        {
            Label* curLabel = static_cast<Label*>(children->values().at(i));
            delete curLabel;
        } else if(dynamic_cast<PushButton*>(children->values().at(i))!=NULL)
        {
            PushButton* curPushButton = static_cast<PushButton*>(children->values().at(i));
            delete curPushButton;
        } else if(dynamic_cast<CheckBox*>(children->values().at(i))!=NULL)
        {
            CheckBox* curCheckBox = static_cast<CheckBox*>(children->values().at(i));
            delete curCheckBox;
        } else if(dynamic_cast<LayoutWidget*>(children->values().at(i))!=NULL)
        {
            LayoutWidget* curLayout = static_cast<LayoutWidget*>(children->values().at(i));
            curLayout->clearChildren();
            delete curLayout;
        }
    }
    children->clear();
    return true;
}

void LayoutWidget::setSelected(bool value)
{
    if(value)
        _selected = true;
    else
        _selected = false;
    this->repaint();
}

void LayoutWidget::setTreeItem(QTreeWidgetItem *item)
{
    if(item!=NULL)
        insepctorItem = item;
}

void LayoutWidget::insertToLayout(QString key, QWidget* wid)
{
    if(type!=WidgetType::Null){
        wid->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
        children->insert(key,wid);
        this->layout()->addWidget(wid);
    } else
        children->insert(key,wid);
}

QWidget* LayoutWidget::takeWidget(QString key)
{
    QWidget* widget;
    if(children->contains(key))
        widget = children->take(key);
    return widget;
}

QWidget *LayoutWidget::insertWidget(QWidget* widget)
{
    if(type!=WidgetType::Null){
        widget->setParent(this);
        widget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
        children->insert(widget->objectName(),widget);
        this->layout()->addWidget(widget);
        widget->setVisible(true);
    } else
    {
        widget->setParent(this);
        children->insert(widget->objectName(),widget);
        widget->setVisible(true);
    }
}

void LayoutWidget::mousePressEvent(QMouseEvent *event)
{
    if(!_selected){
        this->setSelected(true);
        emit selected(this);
    }

    if( event->button() == Qt::RightButton)
    {
        contextMenu->exec(event->globalPos());
        return;
    }

    mousePosX = event->x();
    mousePosY = event->y();

    if (_selected){
        tempRect = geometry();
        if(_lt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeFDiagCursor);
            resizeType = ResizeType::LeftTopResize;
        }
        else if(_lm.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::LeftMidResize;
        }
        else if(_mt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::MidTopResize;
        }
        else if(_rt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeBDiagCursor);
            resizeType = ResizeType::RightTopResize;
        }
        else if(_lb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeBDiagCursor);
            resizeType = ResizeType::LeftBotResize;
        }
        else if(_rb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeFDiagCursor);
            resizeType = ResizeType::RightBotResize;
        }
        else if(_mb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::MidBotResize;
        }
        else if(_rm.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::RightMidResize;
        } else
            drag = true;
    }
}

void LayoutWidget::mouseReleaseEvent(QMouseEvent *)
{
    resizeType = ResizeType::Null;
    drag = false;
    setCursor(Qt::ArrowCursor);
}

void LayoutWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(resizeType!=ResizeType::Null)
        resizeWidget(event);
    else if (drag)
    {
        this->hide();
        QByteArray itemData;
        QDataStream dataStream(&itemData, QIODevice::WriteOnly);
        QMimeData *mimeData = new QMimeData;
        QDrag *dragButton = new QDrag(this);
        this->setFixedS(false);

        QPixmap pixmap(this->size());
        _selected = false;
        this->render(&pixmap);
        _selected = true;
        QPoint hotSpot = event->pos();
        dataStream << hotSpot;

        switch (type) {
        case WidgetType::VBoxLayout:
            mimeData->setData("vlayout", itemData);
            break;
        case WidgetType::HBoxLayout:
            mimeData->setData("hlayout", itemData);
            break;
        case WidgetType::Null:
            mimeData->setData("vlayout", itemData);
            break;
        }

        dragButton->setMimeData(mimeData);
        dragButton->setHotSpot(QPoint(event->pos().x(), event->pos().y()));
        dragButton->setPixmap(pixmap);

        if (dragButton->exec(Qt::MoveAction) == Qt::MoveAction)
            true;
        drag = false;
    }
}

void LayoutWidget::resizeWidget(QMouseEvent *event)
{
    int newX=tempRect.x(), newY=tempRect.y();
    int newWidth = tempRect.width(), newHeight = tempRect.height();

    switch(resizeType){
    case ResizeType::LeftTopResize://+
        newWidth += -(event->pos().x() - mousePosX);
        newHeight += -(event->pos().y() - mousePosY);
        newX+= event->pos().x() - mousePosX;
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::MidTopResize: //+
        newHeight += -(event->pos().y() - mousePosY);
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::RightTopResize: //+
        newWidth += event->pos().x() - mousePosX;
        newHeight += -(event->pos().y() - mousePosY);
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::RightMidResize://+
        newWidth += event->pos().x() - mousePosX;
        break;
    case ResizeType::RightBotResize://+
        newWidth += event->pos().x() - mousePosX;
        newHeight += event->pos().y() - mousePosY;
        break;
    case ResizeType::MidBotResize: //+
        newHeight += event->pos().y() - mousePosY;
        break;
    case ResizeType::LeftBotResize: //+
        newWidth += -(event->pos().x() - mousePosX);
        newHeight += event->pos().y() - mousePosY;
        newX+= event->pos().x() - mousePosX;
        break;
    case ResizeType::LeftMidResize: //+
        newWidth += -(event->x() - mousePosX);
        newX+= event->pos().x() - mousePosX;
        break;
    case ResizeType::Null: //+
        newWidth += event->pos().x() - mousePosX;
        newHeight += event->pos().y() - mousePosY;
        break;
    }

    if(newWidth<20)
        newWidth = 20;
    if(newHeight<20)
        newHeight = 20;

    if(newX > (tempRect.x() + tempRect.width() - 20))
        newX =  tempRect.x() + tempRect.width() - 20;
    if(newY > ( tempRect.y() + tempRect.height() - 20))
        newY =   tempRect.y() + tempRect.height() - 20;

    setFixedSize(newWidth,newHeight);
    move(newX,newY);
    tempRect.setTopLeft(QPoint(newX, newY));
}

void LayoutWidget::setFixedS(bool fix)
{
    if(fix)
    {
        this->setMaximumSize(size());
        this->setMinimumSize(size());
    }
    else
    {
        this->setMinimumSize(QSize(20,20));
        this->setMaximumSize(QSize(16777215,16777215));
    }
}

void LayoutWidget::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);

    QPainter painter(this);
    painter.save();
    QRect thisSize;
    thisSize.setRect(0,0,width()-1,height()-1);
    QPen* bluePen = new QPen(Qt::blue);
    bluePen->setWidth(1);
    painter.setPen(*bluePen);
    painter.drawRect(thisSize);
    if(_selected)
    {
        _lt.setRect(thisSize.x(),
                    thisSize.y(),
                    resiseRectSize,
                    resiseRectSize);
        _lb.setRect(thisSize.x(),
                    thisSize.y() + thisSize.height()-resiseRectSize,
                    resiseRectSize,
                    resiseRectSize);
        _rt.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                    thisSize.y(),
                    resiseRectSize,
                    resiseRectSize);
        _rb.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                    thisSize.y()+thisSize.height()-resiseRectSize,
                    resiseRectSize,
                    resiseRectSize);
        _mt.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                    thisSize.y(),
                    resiseRectSize,
                    resiseRectSize);
        _mb.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                    thisSize.y()+thisSize.height()-resiseRectSize,
                    resiseRectSize,
                    resiseRectSize);
        _rm.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                    thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                    resiseRectSize,
                    resiseRectSize);

        _lm.setRect(thisSize.x(),
                    thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                    resiseRectSize,
                    resiseRectSize);

        QPen* pen = new QPen(Qt::black);
        pen->setWidth(1);
        painter.setPen(*pen);
        painter.setBrush(Qt::green);
        painter.drawRect(_lt);
        painter.drawRect(_lb);
        painter.drawRect(_rt);
        painter.drawRect(_rb);
        painter.drawRect(_mt);
        painter.drawRect(_rm);
        painter.drawRect(_lm);
        painter.drawRect(_mb);
    }
    painter.restore();
}

void LayoutWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("label")
            || event->mimeData()->hasFormat("vlayout")
            || event->mimeData()->hasFormat("hlayout")
            || event->mimeData()->hasFormat("button")
            || event->mimeData()->hasFormat("checkbox"))
    {
        event->accept();
    }
    else
        event->ignore();
}

void LayoutWidget::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasFormat("label")
            || event->mimeData()->hasFormat("vlayout")
            || event->mimeData()->hasFormat("hlayout")
            || event->mimeData()->hasFormat("button")
            || event->mimeData()->hasFormat("checkbox")) {
        WidgetType type = WidgetType::Null;
        QPoint newPos = QPoint(event->pos().x(), event->pos().y());
        if (event->mimeData()->hasFormat("label")) {
            type = WidgetType::Label;
            if(dynamic_cast<Label*>(event->source())!=NULL)
            {
                event->setDropAction(Qt::MoveAction);
                event->accept();
                emit draggingToForm(this, type, newPos, static_cast<Label*>(event->source()));
                return;
            }
        } else if (event->mimeData()->hasFormat("button")) {
            type = WidgetType::PushButton;
            if(dynamic_cast<PushButton*>(event->source())!=NULL)
            {
                event->setDropAction(Qt::MoveAction);
                event->accept();
                emit draggingToForm(this, type, newPos, static_cast<PushButton*>(event->source()));
                return;
            }
        } else if (event->mimeData()->hasFormat("checkbox")) {
            type = WidgetType::CheckBox;
            if(dynamic_cast<CheckBox*>(event->source())!=NULL)
            {
                event->setDropAction(Qt::MoveAction);
                event->accept();
                emit draggingToForm(this, type, newPos, static_cast<CheckBox*>(event->source()));
                return;
            }
        } else if (event->mimeData()->hasFormat("vlayout")) {
            type = WidgetType::VBoxLayout;
            if(dynamic_cast<LayoutWidget*>(event->source())!=NULL)
            {
                event->setDropAction(Qt::MoveAction);
                event->accept();
                emit draggingToForm(this, type, newPos, static_cast<LayoutWidget*>(event->source()));
                return;
            }
        } else if (event->mimeData()->hasFormat("hlayout")) {
            type = WidgetType::HBoxLayout;
            if(dynamic_cast<LayoutWidget*>(event->source())!=NULL)
            {
                event->setDropAction(Qt::MoveAction);
                event->accept();
                emit draggingToForm(this, type, newPos, static_cast<LayoutWidget*>(event->source()));
                return;
            }
        }
        event->setDropAction(Qt::MoveAction);
        event->accept();

        emit addingToLayout(this, type, newPos);
    } else {
        event->ignore();
    }
}

