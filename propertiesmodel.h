#ifndef PROPERTIESMODEL_H
#define PROPERTIESMODEL_H

#include <QAbstractTableModel>
#include <QVariant>
#include <QDebug>
#include "label.h"
#include "pushbutton.h"
#include "checkbox.h"
#include "enums.h"

class PropertiesModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit PropertiesModel(QWidget* newParentWidget, QObject *parent = 0);
    ~PropertiesModel();

    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    int rows;
    QWidget* parentWidget;
    WidgetType parentWidgetType;
    void initParentWidgetType();

signals:

private slots:
    changeParentWidget(QModelIndex topLeft, QModelIndex botRight, QVector<int> val);
};

#endif // PROPERTIESMODEL_H
