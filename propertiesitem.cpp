#include <QStringList>

#include "propertiesitem.h"

PropertiesItem::PropertiesItem(QString newFirstColumnData, QString newSecondColumnData,PropertiesItem *parent)
{
    m_parentItem = parent;
    firstColumnData = newFirstColumnData;
    secondColumnData = newSecondColumnData;
}

PropertiesItem::~PropertiesItem()
{
    qDeleteAll(m_childItems);
}

void PropertiesItem::appendChild(PropertiesItem *item)
{
    m_childItems.append(item);
}

PropertiesItem *PropertiesItem::child(int row)
{
    return m_childItems.value(row);
}

int PropertiesItem::childCount() const
{
    return m_childItems.count();
}

int PropertiesItem::columnCount() const
{
    return 2;
}

QVariant PropertiesItem::data(int column) const
{
    if(column==0)
        return firstColumnData;
    else
        return secondColumnData;
}

PropertiesItem *PropertiesItem::parent()
{
    return m_parentItem;
}

bool PropertiesItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= 2)
        return false;

    if(column == 0)
        firstColumnData = value.toString();
    else
        secondColumnData = value.toString();

    return true;
}

int PropertiesItem::row() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<PropertiesItem*>(this));

    return 0;
}
