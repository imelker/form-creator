#ifndef PROPERTIESITEM_H
#define PROPERTIESITEM_H

#include <QList>
#include <QVariant>
#include <QTreeWidgetItem>

class PropertiesItem : public QTreeWidgetItem
{
public:
    explicit PropertiesItem(QString newFirstColumnData, QString newSecondColumnData, PropertiesItem *parentItem = 0);
    ~PropertiesItem();

    void appendChild(PropertiesItem *child);

    PropertiesItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    bool setData(int column, const QVariant &value);
    int row() const;
    int childNumber() const;
    PropertiesItem *parent();

private:
    QList<PropertiesItem*> m_childItems;
    PropertiesItem *m_parentItem;
    QString firstColumnData;
    QString secondColumnData;
};

#endif // PROPERTIESITEM_H
