#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    enum class ResizeType{Null,LeftTopResize,RightTopResize,MidTopResize,LeftBotResize,RightBotResize,MidBotResize,LeftMidResize,RightMidResize};

private:
        QString objectName;
public:
        void setObjectName(QString text);
        QString getObjectName(){return objectName;}

signals:
    void widgetChanged();
public slots:
};

#endif // WIDGET_H
