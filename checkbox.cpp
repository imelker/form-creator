#include "checkbox.h"
#include <QMimeData>
#include <QDrag>
#include <QMenu>

CheckBox::CheckBox(QWidget *parent) : QCheckBox(parent)
{
    thisID = countID(0,-1);
    if(thisID!=0)
        setObjectName("checkBox_" + QString::number(thisID));
    else
        setObjectName("checkBox");

    //Настройки CheckBox'a
    _selected = false;
    type = WidgetType::CheckBox;
    iconPath = "";
    resizeType = ResizeType::Null;
    resiseRectSize = 5;
    drag = false;
    contextMenu = new QMenu(this);

    this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    this->setText("CheckBox");

    //CheckBox без текста и LineEdit
    editCb = new QCheckBox(this); //CheckBox
    editCb->setEnabled(false);
    editCb->setGeometry(0,0,15,this->height());
    editCb->setVisible(false);
    edit = new QLineEdit(this); //LineEidt
    edit->setGeometry(15,resiseRectSize,this->width()- editCb->width(),this->height()-2*resiseRectSize);
    edit->setVisible(false);

    connect(edit,SIGNAL(editingFinished()),this,SLOT(changeText())); //при конце изменений в lineEdit, апдейтит чекбокс.
}

CheckBox::~CheckBox()
{
    countID(1,thisID);
}

void CheckBox::setCheckBoxIconPath(QString path)
{
    iconPath = path;
    setIcon(QIcon(iconPath));
}

void CheckBox::changeText()
{
    setText(edit->text());
    editCb->setVisible(false);
    edit->setVisible(false);
    emit textChanged(this);
}

void CheckBox::mouseDoubleClickEvent(QMouseEvent *e)
{
    //todo добавить то, что это правая кнопка мыши
    if(_selected)
    {
        editCb->setGeometry(0,0,15,this->height());
        edit->setGeometry(15,resiseRectSize,this->width()- editCb->width(),this->height()-2*resiseRectSize);
        editCb->setVisible(true);
        edit->setVisible(true);
        editCb->setChecked(isChecked());
        edit->setText(text());
    }
    repaint();
}


void CheckBox::setSelected(bool value)
{
    if(value)
        _selected = true;
    else
    {
        _selected = false;
        edit->setVisible(false);
        editCb->setVisible(false);
    }
    this->repaint();
}

void CheckBox::setTreeItem(QTreeWidgetItem *item)
{
    if(item!=NULL)
        insepctorItem = item;
}

void CheckBox::setFixedS(bool fix)
{
    _fixedBySize = fix;
    if(fix)
    {
        this->setMaximumSize(size());
        this->setMinimumSize(size());
    }
    else
    {
        this->setMinimumSize(QSize(20,20));
        this->setMaximumSize(QSize(16777215,16777215));
    }
}

void CheckBox::mousePressEvent(QMouseEvent *event)
{
    if(!_selected){
        this->setSelected(true);
        emit selected(this);
    }

    if( event->button() == Qt::RightButton)
    {
        contextMenu->exec(event->globalPos());
        return;
    }

    mousePosX = event->x();
    mousePosY = event->y();
    if (_selected){
        tempRect = geometry();
        if(_lt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeFDiagCursor);
            resizeType = ResizeType::LeftTopResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        }
        else if(_lm.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::LeftMidResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        }
        else if(_mt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::MidTopResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        }
        else if(_rt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeBDiagCursor);
            resizeType = ResizeType::RightTopResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        }
        else if(_lb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeBDiagCursor);
            resizeType = ResizeType::LeftBotResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        }
        else if(_rb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeFDiagCursor);
            resizeType = ResizeType::RightBotResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        }
        else if(_mb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::MidBotResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        }
        else if(_rm.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::RightMidResize;
            edit->setVisible(false);
            editCb->setVisible(false);
        } else if(!edit->isVisible() && !editCb->isVisible())
            drag = true;
    }
}

void CheckBox::mouseReleaseEvent(QMouseEvent *event)
{
    resizeType = ResizeType::Null;
    drag = false;
    setCursor(Qt::ArrowCursor);
}

void CheckBox::mouseMoveEvent(QMouseEvent *event)
{
    if(resizeType!=ResizeType::Null)
        resizeWidget(event);
    else if (drag)
    {
        this->hide();
        QByteArray itemData;
        QDataStream dataStream(&itemData, QIODevice::WriteOnly);
        QMimeData *mimeData = new QMimeData;
        QDrag *dragButton = new QDrag(this);
        this->setFixedS(false);

        QPixmap pixmap(this->size());
        _selected = false;
        this->render(&pixmap);
        _selected = true;
        QPoint hotSpot = event->pos();
        dataStream << hotSpot;

        mimeData->setData("checkbox", itemData);

        dragButton->setMimeData(mimeData);
        dragButton->setHotSpot(QPoint(event->pos().x(), event->pos().y()));
        dragButton->setPixmap(pixmap);

        if (dragButton->exec(Qt::MoveAction) == Qt::MoveAction)
            true;
        drag = false;
    }
}

void CheckBox::resizeWidget(QMouseEvent *event)
{
    int newX=tempRect.x(), newY=tempRect.y();
    int newWidth = tempRect.width(), newHeight = tempRect.height();

    switch(resizeType){
    case ResizeType::LeftTopResize://+
        newWidth += -(event->pos().x() - mousePosX);
        newHeight += -(event->pos().y() - mousePosY);
        newX+= event->pos().x() - mousePosX;
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::MidTopResize: //+
        newHeight += -(event->pos().y() - mousePosY);
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::RightTopResize: //+
        newWidth += event->pos().x() - mousePosX;
        newHeight += -(event->pos().y() - mousePosY);
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::RightMidResize://+
        newWidth += event->pos().x() - mousePosX;
        break;
    case ResizeType::RightBotResize://+
        newWidth += event->pos().x() - mousePosX;
        newHeight += event->pos().y() - mousePosY;
        break;
    case ResizeType::MidBotResize: //+
        newHeight += event->pos().y() - mousePosY;
        break;
    case ResizeType::LeftBotResize: //+
        newWidth += -(event->pos().x() - mousePosX);
        newHeight += event->pos().y() - mousePosY;
        newX+= event->pos().x() - mousePosX;
        break;
    case ResizeType::LeftMidResize: //+
        newWidth += -(event->x() - mousePosX);
        newX+= event->pos().x() - mousePosX;
        break;
    }

    if(newWidth<20)
        newWidth = 20;
    if(newHeight<20)
        newHeight = 20;

    if(newX > (tempRect.x() + tempRect.width() - 20))
        newX =  tempRect.x() + tempRect.width() - 20;
    if(newY > ( tempRect.y() + tempRect.height() - 20))
        newY =   tempRect.y() + tempRect.height() - 20;

    setFixedSize(newWidth,newHeight);
    move(newX,newY);
    tempRect.setTopLeft(QPoint(newX, newY));
}

void CheckBox::paintEvent(QPaintEvent *event)
{
    QCheckBox::paintEvent(event);

    QPainter painter(this);
    painter.save();

    if(_selected)
    {
    painter.setPen(Qt::green);
    QRect thisSize;
    thisSize.setRect(0,0,width()-1,height()-1);

    _lt.setRect(thisSize.x(),thisSize.y(),resiseRectSize,resiseRectSize);
    _lb.setRect(thisSize.x(),thisSize.y() + thisSize.height()-resiseRectSize,resiseRectSize,resiseRectSize);
    _rt.setRect(thisSize.x()+thisSize.width()-resiseRectSize,thisSize.y(),resiseRectSize,resiseRectSize);
    _rb.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y()+thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _mt.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                thisSize.y(),
                resiseRectSize,
                resiseRectSize);
    _mb.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                thisSize.y()+thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _rm.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                resiseRectSize,
                resiseRectSize);

    _lm.setRect(thisSize.x(),
                thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                resiseRectSize,
                resiseRectSize);

        QPen* pen = new QPen(Qt::black);
        pen->setWidth(1);
        painter.setPen(*pen);
        painter.setBrush(Qt::green);
        painter.drawRect(_lt);
        painter.drawRect(_lb);
        painter.drawRect(_rt);
        painter.drawRect(_rb);
        painter.drawRect(_mt);
        painter.drawRect(_rm);
        painter.drawRect(_lm);
        painter.drawRect(_mb);
    }
    painter.restore();
}

