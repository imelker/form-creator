#ifndef CHECKBOX_H
#define CHECKBOX_H

#include <QStackedWidget>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QLayout>
#include <QFrame>
#include <QTreeWidget>
#include <QAbstractItemModel>
#include "enums.h"

class CheckBox : public QCheckBox
{
    Q_OBJECT
    static int countID(const int flag, const int id) //flag: 0 - обычная работа, 1 вызов при delete
    {
        static QVector<int> _ids;
        if(flag == 0){
            if(_ids.isEmpty())
                _ids.append(_ids.size());
            else if (_ids.last()!=-1)
                _ids.append(_ids.size());

            for(int i = 0; i < _ids.size();i++)
            {
                if(_ids.at(i)==-1){
                    _ids.removeAt(i);
                    _ids.insert(i,i);
                    qDebug() << _ids.at(i)<< " - " << i;
                    return i;
                } else if (i==_ids.size()-1)
                {
                    return i;
                }
            }
        } else if (1)
        {
            _ids.removeAt(id);
            _ids.insert(id,-1);
            return -1;
        }
    }
public:
    explicit CheckBox(QWidget *parent=0);
    ~CheckBox();
    QMenu* contextMenu;

private:
    QLineEdit* edit;
    QCheckBox* editCb;
    QString iconPath;
    QTreeWidgetItem* insepctorItem;
    bool _selected;
    bool _fixedBySize;
    int thisID;

    bool drag;
    int mousePosX,mousePosY;
    ResizeType resizeType;
    QRect _lt,_rt,_mt,_lb,_rb,_mb,_lm,_rm;
    QRect tempRect;
    int resiseRectSize;
    void resizeWidget(QMouseEvent *event);

public:
    WidgetType type;
    bool isSelected(){return _selected;}

    //сеттеры и геттеры
    void setCheckBoxIconPath(QString path);
    void setSelected(bool value);
    void setTreeItem(QTreeWidgetItem* item);
    void setFixedS(bool fix);
    bool isFixedBySide(){return _fixedBySize;}
    QTreeWidgetItem* getTreeItem(){return insepctorItem;}
    QString getCheckBoxIconPath(){return iconPath;}

signals:
    void selected(QWidget*);
    void textChanged(QWidget*);

private slots:
    void changeText();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
};

#endif // CHECKBOX_H
