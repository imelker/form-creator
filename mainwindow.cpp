#include "mainwindow.h"
#include <QStandardItemModel>
#include <QFileDialog>
#include <QMessageBox>
#include <QStyledItemDelegate>
#include <QFile>
#include <QClipboard>
#include <QSpinBox>
#include <QStyleFactory>
#include "ui_mainwindow.h"
#include "propertiesmodel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //настройка окна сообщений
    ui->text_msg->setReadOnly(true);
    ui->text_msg->setPlainText("- Программа запущена.\n");

    //установка формы
    windowFrame = new FormWidget(ui->scrollAreaWidgetContents);
    windowFrame->setNullLayout();
    ui->scrollAreaWidgetContents->layout()->addWidget(windowFrame);

    //контейнер с лейаутами
    setCurWidget(NULL);

    initToolBar();
    initMenu();

    //connect(this,SIGNAL(contentChanged()),windowFrame,SLOT(setUnSelected()));
    //connect(windowFrame,SIGNAL(selected(QWidget*)),this,SLOT(cancelSelection()));
    connect(windowFrame,SIGNAL(selected(QWidget*)),this,SLOT(widgetSelected(QWidget*)));

    initEditor(); //инструменты для codeEditor

    inspector = ui->tree_inspector;
    inspector->setHeaderHidden(false);
    inspector->expandAll();

    properties = ui->table_properties;
    properties->verticalHeader()->setDefaultSectionSize(20);
    properties->horizontalHeader()->setStretchLastSection(true);
    properties->setHorizontalHeaderLabels({"Параметр","Значение"});

    rewriteCode();
    repaintTree();

    connect(ui->dock_inspector,SIGNAL(visibilityChanged(bool)),ui->act_viewinspector,SLOT(setChecked(bool)));
    connect(ui->dock_prop,SIGNAL(visibilityChanged(bool)),ui->act_viewporperties,SLOT(setChecked(bool)));
    connect(ui->dock_widgets,SIGNAL(visibilityChanged(bool)),ui->act_viewwidget,SLOT(setChecked(bool)));
    connect(windowFrame,SIGNAL(draggingToForm(QWidget*,WidgetType,QPoint,QWidget*)),this,SLOT(dragToFrame(QWidget*,WidgetType,QPoint,QWidget*)));
    connect(windowFrame,SIGNAL(addingToForm(QWidget*,WidgetType,QPoint)),this,SLOT(addToFrame(QWidget*,WidgetType,QPoint)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initMenu()
{
    //Файл
    connect(ui->act_new, SIGNAL(triggered(bool)), this, SLOT(newFile()));
    connect(ui->act_open, SIGNAL(triggered(bool)), this, SLOT(openFile()));
    connect(ui->act_save, SIGNAL(triggered(bool)), this, SLOT(saveFile()));
    connect(ui->act_saveas, SIGNAL(triggered(bool)), this, SLOT(saveFileAs()));
    connect(ui->act_parce, SIGNAL(triggered(bool)), this, SLOT(parceCode()));
    connect(ui->act_exit, SIGNAL(triggered(bool)), this, SLOT(close()));

    //Правка
    //connect(ui->act_cancel, SIGNAL(triggered(bool)), this, SLOT());
    //connect(ui->act_redo, SIGNAL(triggered(bool)), this, SLOT());
    connect(ui->act_cut, SIGNAL(triggered(bool)), this, SLOT(cutWidget()));
    connect(ui->act_copy, SIGNAL(triggered(bool)), this, SLOT(copyWidget()));
    connect(ui->act_paste, SIGNAL(triggered(bool)), this, SLOT(pasteWidget()));
    connect(ui->act_del, SIGNAL(triggered(bool)), this, SLOT(delWidget()));
    connect(ui->act_hlayout, SIGNAL(triggered(bool)), windowFrame, SLOT(setHBoxLayout()));
    connect(ui->act_vlayout, SIGNAL(triggered(bool)), windowFrame, SLOT(setVBoxLayout()));
    connect(ui->act_dellayout, SIGNAL(triggered(bool)), windowFrame, SLOT(setNullLayout()));

    //Окно
    connect(ui->act_fullscreen, SIGNAL(triggered(bool)), this, SLOT(setFullScreen()));
    connect(ui->act_viewwidget, SIGNAL(triggered(bool)), this, SLOT(viewWidgets(bool)));
    connect(ui->act_viewinspector, SIGNAL(triggered(bool)), this, SLOT(viewInspector(bool)));
    connect(ui->act_viewporperties, SIGNAL(triggered(bool)), this, SLOT(viewProperties(bool)));
    connect(ui->act_lightstyle, SIGNAL(triggered(bool)), this, SLOT(setLightStyle(bool)));
    connect(ui->act_darkstyle, SIGNAL(triggered(bool)), this, SLOT(setDarkStyle(bool)));

    //Справка
    connect(ui->act_help, SIGNAL(triggered(bool)), this, SLOT(helpDialog()));
    connect(ui->act_info, SIGNAL(triggered(bool)), this, SLOT(infoDialog()));
}

void MainWindow::initToolBar()
{
    QToolBar* toolBar = new QToolBar("Инструменты");
    toolBar->setIconSize(QSize(16,16));
    QAction *delAction = toolBar->addAction(QIcon(":/new/icons/icons/delwidget.png"),"");
    delAction->setToolTip("Удалить");
    delAction ->setShortcut(QKeySequence(Qt::Key_Delete));
    connect(delAction,SIGNAL(triggered(bool)),this,SLOT(delWidget()));
    toolBar->addSeparator();

    QAction *hlayoutAction = toolBar->addAction(QIcon(":/new/icons/icons/hboxlayout.png"),"");
    hlayoutAction->setToolTip("Скомпоновать по горизонтали");
    connect(hlayoutAction,SIGNAL(triggered(bool)),windowFrame,SLOT(setHBoxLayout()));
    connect(hlayoutAction,SIGNAL(triggered(bool)),this,SLOT(repaintTree()));

    QAction *vlayoutAction = toolBar->addAction(QIcon(":/new/icons/icons/vboxlayout.png"),"");
    vlayoutAction->setToolTip("Скомпоновать по вертикали");
    connect(vlayoutAction,SIGNAL(triggered(bool)),windowFrame,SLOT(setVBoxLayout()));
    connect(vlayoutAction,SIGNAL(triggered(bool)),this,SLOT(repaintTree()));

    QAction *delLayoutAction =  toolBar->addAction(QIcon(":/new/icons/icons/nolayout.png"),"");
    delLayoutAction->setToolTip("Удалить компановщик");
    connect(delLayoutAction,SIGNAL(triggered(bool)),windowFrame,SLOT(setNullLayout()));
    connect(delLayoutAction,SIGNAL(triggered(bool)),this,SLOT(repaintTree()));

    ui->toolBarLayout->addWidget(toolBar);
}

void MainWindow::initEditor()
{
    filePath = "";
    QToolBar* codeEditorTB = new QToolBar("Редактор кода");
    codeEditorTB->setIconSize(QSize(16,16));
    QAction *newAction = codeEditorTB->addAction(QIcon(":/new/icons/icons/new.png"),"");
    newAction->setToolTip("Новый файл");
    connect(newAction, SIGNAL(triggered()), this, SLOT(newFile()));

    QAction *openAction = codeEditorTB->addAction(QIcon(":/new/icons/icons/open.png"),"");
    openAction->setToolTip("Открыть файл");
    connect(openAction, SIGNAL(triggered()), this, SLOT(openFile()));

    QAction *saveAction = codeEditorTB->addAction(QIcon(":/new/icons/icons/save.png"),"");
    saveAction->setToolTip("Сохранить файл");
    connect(saveAction, SIGNAL(triggered()), this, SLOT(saveFile()));

    QAction *saveAsAction = codeEditorTB->addAction(QIcon(":/new/icons/icons/saveas.png"),"");
    saveAsAction->setToolTip("Сохранить файл как");
    connect(saveAsAction, SIGNAL(triggered()), this, SLOT(saveFileAs()));
    codeEditorTB->addSeparator();

    QAction *compileAction =  codeEditorTB->addAction(QIcon(":/new/icons/icons/compile.png"),"");
    compileAction->setToolTip("Применить код");
    connect(compileAction, SIGNAL(triggered()), this, SLOT(parceCode()));

    ui->tab_code->layout()->addWidget(codeEditorTB);
    text_code = new CodeEditor(this);
    ui->tab_code->layout()->addWidget(text_code);
}

void MainWindow::addLayout(QWidget* parentWidget, QString text, WidgetType type, QPoint pos){
    LayoutWidget* newLayout = addLayoutWhileParce(parentWidget, text, type, pos);
    newLayout->contextMenu->addAction(ui->act_cut);
    newLayout->contextMenu->addAction(ui->act_copy);
    newLayout->contextMenu->addAction(ui->act_del);
    newLayout->contextMenu->addSeparator();
    QAction *raiseAction = newLayout->contextMenu->addAction("На передний план");
    connect(raiseAction,SIGNAL(triggered(bool)),newLayout,SLOT(raise()));

    rewriteCode();
    repaintTree();
    reloadTable(newLayout);
    newLayout->setSelected(true);

    SendMSG("- - - Добавлен новый Layout("+ newLayout->objectName() +": "+ QString::number(newLayout->pos().rx()) + ", " + QString::number(newLayout->pos().ry())+").");
}

void MainWindow::addLabel(QWidget* parentWidget, QString text, QPoint pos){

    Label* newLabel= addLabelWhileParce(parentWidget,text, pos);
    newLabel->contextMenu->addAction(ui->act_cut);
    newLabel->contextMenu->addAction(ui->act_copy);
    newLabel->contextMenu->addAction(ui->act_del);
    newLabel->contextMenu->addSeparator();
    QAction *raiseAction = newLabel->contextMenu->addAction("На передний план");
    connect(raiseAction,SIGNAL(triggered(bool)),newLabel,SLOT(raise()));
    rewriteCode();
    repaintTree();
    reloadTable(newLabel);
    newLabel->setSelected(true);
    SendMSG("- - Добавлен новый Label("+ newLabel->objectName() +": "+ QString::number(newLabel->pos().rx()) + ", " + QString::number(newLabel->pos().ry())+").");
}

void MainWindow::addCheckBox(QWidget* parentWidget, QString text, QPoint pos){
    CheckBox* newCheckBox = addCheckBoxWhileParce(parentWidget, text,pos);
    newCheckBox->contextMenu->addAction(ui->act_cut);
    newCheckBox->contextMenu->addAction(ui->act_copy);
    newCheckBox->contextMenu->addAction(ui->act_del);
    newCheckBox->contextMenu->addSeparator();
    QAction *raiseAction = newCheckBox->contextMenu->addAction("На передний план");
    connect(raiseAction,SIGNAL(triggered(bool)),newCheckBox,SLOT(raise()));
    rewriteCode();
    repaintTree();
    reloadTable(newCheckBox);
    newCheckBox->setSelected(true);
    SendMSG("- - Добавлен новый CheckBox("+ newCheckBox->objectName() +": "+ QString::number(newCheckBox->pos().rx()) + ", " + QString::number(newCheckBox->pos().ry())+").");
}

void MainWindow::addPushButton(QWidget* parentWidget, QString text, QPoint pos){
    PushButton* newPushButton = addPushButtonWhileParce(parentWidget, text, pos);
    newPushButton->contextMenu->addAction(ui->act_cut);
    newPushButton->contextMenu->addAction(ui->act_copy);
    newPushButton->contextMenu->addAction(ui->act_del);
    newPushButton->contextMenu->addSeparator();
    QAction *raiseAction = newPushButton->contextMenu->addAction("На передний план");
    connect(raiseAction,SIGNAL(triggered(bool)),newPushButton,SLOT(raise()));
    rewriteCode();
    repaintTree();
    reloadTable(newPushButton);
    newPushButton->setSelected(true);
    SendMSG("- - Добавлен новый PushButton("+ newPushButton->objectName() +": "+ QString::number(newPushButton->pos().rx()) + ", " + QString::number(newPushButton->pos().ry())+").");
}

LayoutWidget* MainWindow::addLayoutWhileParce(QWidget* parentWidget, QString text, WidgetType type, QPoint pos){
    LayoutWidget* newLayout= new LayoutWidget(type, parentWidget);
    if(text!=NULL)
        newLayout->setObjectName(text);
    setCurWidget(newLayout);

    if(!pos.isNull()){
        QPoint* newPos =  new QPoint(pos.x() - newLayout->width()/2, pos.y() - newLayout->height()/2);
        newLayout->setGeometry(newPos->rx(),newPos->ry(), newLayout->width(), newLayout->height());
    }

    if (dynamic_cast<LayoutWidget*>(parentWidget)!=NULL)
    {
        LayoutWidget* layoutWidget = static_cast<LayoutWidget*>(parentWidget);
        layoutWidget->insertToLayout(newLayout->objectName(),newLayout);
    }
    else if(dynamic_cast<FormWidget*>(parentWidget)!=NULL)
    {
        if(windowFrame->layoutType!=WidgetType::Null){
            newLayout->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
            windowFrame->layoutChildren()->insert(newLayout->objectName(),newLayout);
            windowFrame->layout()->addWidget(newLayout);
        } else
            windowFrame->layoutChildren()->insert(newLayout->objectName(),newLayout);
    }

    connect(newLayout,SIGNAL(addingToLayout(QWidget*,WidgetType,QPoint)),this,SLOT(addToFrame(QWidget*,WidgetType,QPoint)));
    connect(newLayout,SIGNAL(draggingToForm(QWidget*,WidgetType,QPoint,QWidget*)),this,SLOT(dragToFrame(QWidget*,WidgetType,QPoint,QWidget*)));
    connect(newLayout,SIGNAL(selected(QWidget*)),this,SLOT(widgetSelected(QWidget*)));

    newLayout->setVisible(true);
    return newLayout;
}

Label* MainWindow::addLabelWhileParce(QWidget* parentWidget, QString text, QPoint pos){

    Label* newLabel= new Label(parentWidget);
    if(text!=NULL)
        newLabel->setObjectName(text);
    setCurWidget(newLabel);

    if(!pos.isNull()){
        QPoint* newPos =  new QPoint(pos.x() - newLabel->width()/2, pos.y() - newLabel->height()/2);
        newLabel->setGeometry(newPos->rx(),newPos->ry(), newLabel->width(), newLabel->height());
    }

    if (dynamic_cast<LayoutWidget*>(parentWidget)!=NULL)
    {
        LayoutWidget* layoutWidget = static_cast<LayoutWidget*>(parentWidget);
        layoutWidget->insertToLayout(newLabel->objectName(),newLabel);
    }
    else if(dynamic_cast<FormWidget*>(parentWidget)!=NULL)
    {
        if(windowFrame->layoutType!=WidgetType::Null){
            newLabel->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
            windowFrame->layoutChildren()->insert(newLabel->objectName(),newLabel);
            windowFrame->layout()->addWidget(newLabel);
        } else
            windowFrame->layoutChildren()->insert(newLabel->objectName(),newLabel);
    }

    connect(newLabel,SIGNAL(selected(QWidget*)),this,SLOT(widgetSelected(QWidget*)));
    connect(newLabel,SIGNAL(textChanged(QWidget*)),this,SLOT(reloadTable(QWidget*)));
    connect(newLabel,SIGNAL(textChanged(QWidget*)),this,SLOT(rewriteCode()));
    newLabel->setVisible(true);
    return newLabel;
}

CheckBox* MainWindow::addCheckBoxWhileParce(QWidget* parentWidget, QString text, QPoint pos){

    CheckBox* newCheckBox= new CheckBox(parentWidget);
    if(text!=NULL)
        newCheckBox->setObjectName(text);
    setCurWidget(newCheckBox);

    if(!pos.isNull()){
        QPoint* newPos =  new QPoint(pos.x() - newCheckBox->width()/2, pos.y() - newCheckBox->height()/2);
        newCheckBox->setGeometry(newPos->rx(),newPos->ry(), newCheckBox->width(), newCheckBox->height());
    }

    if (dynamic_cast<LayoutWidget*>(parentWidget)!=NULL)
    {
        LayoutWidget* layoutWidget = static_cast<LayoutWidget*>(parentWidget);
        layoutWidget->insertToLayout(newCheckBox->objectName(),newCheckBox);
    }
    else if(dynamic_cast<FormWidget*>(parentWidget)!=NULL)
    {
        if(windowFrame->layoutType!=WidgetType::Null){
            newCheckBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
            windowFrame->layoutChildren()->insert(newCheckBox->objectName(),newCheckBox);
            windowFrame->layout()->addWidget(newCheckBox);
        } else
            windowFrame->layoutChildren()->insert(newCheckBox->objectName(),newCheckBox);
    }

    connect(newCheckBox,SIGNAL(selected(QWidget*)),this,SLOT(widgetSelected(QWidget*)));
    connect(newCheckBox,SIGNAL(textChanged(QWidget*)),this,SLOT(reloadTable(QWidget*)));
    connect(newCheckBox,SIGNAL(textChanged(QWidget*)),this,SLOT(rewriteCode()));

    newCheckBox->setVisible(true);
    return newCheckBox;
}

PushButton* MainWindow::addPushButtonWhileParce(QWidget* parentWidget, QString text, QPoint pos){

    PushButton* newPushButton = new PushButton(parentWidget);
    if(text!=NULL)
        newPushButton->setObjectName(text);
    setCurWidget(newPushButton);

    if(!pos.isNull()){
        QPoint* newPos =  new QPoint(pos.x() - newPushButton->width()/2, pos.y() - newPushButton->height()/2);
        newPushButton->setGeometry(newPos->rx(),newPos->ry(), newPushButton->width(), newPushButton->height());
    }

    if (dynamic_cast<LayoutWidget*>(parentWidget)!=NULL)
    {
        LayoutWidget* layoutWidget = static_cast<LayoutWidget*>(parentWidget);
        layoutWidget->insertToLayout(newPushButton->objectName(),newPushButton);
    }
    else if(dynamic_cast<FormWidget*>(parentWidget)!=NULL)
    {
        if(windowFrame->layoutType!=WidgetType::Null){
            newPushButton->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
            windowFrame->layoutChildren()->insert(newPushButton->objectName(),newPushButton);
            windowFrame->layout()->addWidget(newPushButton);
        } else
            windowFrame->layoutChildren()->insert(newPushButton->objectName(),newPushButton);
    }

    connect(newPushButton,SIGNAL(selected(QWidget*)),this,SLOT(widgetSelected(QWidget*)));
    connect(newPushButton,SIGNAL(textChanged(QWidget*)),this,SLOT(reloadTable(QWidget*)));
    connect(newPushButton,SIGNAL(textChanged(QWidget*)),this,SLOT(rewriteCode()));

    newPushButton->setVisible(true);
    return newPushButton;
}

void MainWindow::rewriteCode() //layoutlist -> to xml
{
    code.clear();
    text_code->clear();
    QDomElement root = code.createElement("ui");
    root.setAttribute("width",windowFrame->width());
    root.setAttribute("height",windowFrame->height());
    switch(windowFrame->layoutType){
    case WidgetType::Null:
        root.setAttribute("layoutType","NULL");
        break;
    case WidgetType::VBoxLayout:
        root.setAttribute("layoutType","VBoxLayout");
        break;
    case WidgetType::HBoxLayout:
        root.setAttribute("layoutType","HBoxLayout");
        break;
    default:
        break;
    }
    code.appendChild(root);

    for(int j=0; j < windowFrame->layoutChildren()->size();j++)
    {
        QDomElement widget;
        if(dynamic_cast<Label*>(windowFrame->layoutChildren()->values().at(j))!=NULL){
            Label* label = static_cast<Label*>(windowFrame->layoutChildren()->values().at(j));
            widget = code.createElement("label");
            widget.setAttribute("objectName",label->objectName());
            widget.setAttribute("toolTip",label->toolTip());
            widget.setAttribute("enabled",label->isEnabled());
            QDomElement geometry = code.createElement("geometry");
            geometry.setAttribute("posX",label->pos().x());
            geometry.setAttribute("posY",label->pos().y());
            geometry.setAttribute("width",label->width());
            geometry.setAttribute("height",label->height());
            widget.appendChild(geometry);
            QDomElement text = code.createElement("content");
            text.setAttribute("text",label->text());
            widget.appendChild(text);
        } else if(dynamic_cast<PushButton*>(windowFrame->layoutChildren()->values().at(j))!=NULL){
            PushButton* bt = static_cast<PushButton*>(windowFrame->layoutChildren()->values().at(j));
            widget = code.createElement("pushButton");
            widget.setAttribute("objectName",bt->objectName());
            widget.setAttribute("toolTip",bt->toolTip());
            widget.setAttribute("enabled",bt->isEnabled());
            QDomElement geometry = code.createElement("geometry");
            geometry.setAttribute("posX",bt->pos().x());
            geometry.setAttribute("posY",bt->pos().y());
            geometry.setAttribute("width",bt->width());
            geometry.setAttribute("height",bt->height());
            widget.appendChild(geometry);
            QDomElement text = code.createElement("content");
            text.setAttribute("text",bt->text());
            widget.appendChild(text);
        } else if(dynamic_cast<CheckBox*>(windowFrame->layoutChildren()->values().at(j))!=NULL){
            CheckBox* cb = static_cast<CheckBox*>(windowFrame->layoutChildren()->values().at(j));
            widget = code.createElement("checkBox");
            widget.setAttribute("objectName",cb->objectName());
            widget.setAttribute("isChecked",cb->isChecked());
            widget.setAttribute("toolTip",cb->toolTip());
            widget.setAttribute("enabled",cb->isEnabled());
            QDomElement geometry = code.createElement("geometry");
            geometry.setAttribute("posX",cb->pos().x());
            geometry.setAttribute("posY",cb->pos().y());
            geometry.setAttribute("width",cb->width());
            geometry.setAttribute("height",cb->height());
            widget.appendChild(geometry);
            QDomElement text = code.createElement("content");
            text.setAttribute("text",cb->text());
            widget.appendChild(text);
        } else if(dynamic_cast<LayoutWidget*>(windowFrame->layoutChildren()->values().at(j))!=NULL){
            LayoutWidget* lw = static_cast<LayoutWidget*>(windowFrame->layoutChildren()->values().at(j));
            widget = layoutToXML(lw,root);
        }
        root.appendChild(widget);
    }
    text_code->setPlainText(code.toString());
}

QDomElement MainWindow::layoutToXML(LayoutWidget* testL,QDomElement root)
{

    QDomElement layout = code.createElement("layout");
    switch(testL->type){
    case WidgetType::Null:
        layout.setAttribute("layoutType","NULL");
        break;
    case WidgetType::VBoxLayout:
        layout.setAttribute("layoutType","VBoxLayout");
        break;
    case WidgetType::HBoxLayout:
        layout.setAttribute("layoutType","HBoxLayout");
        break;
    default:
        break;
    }
    layout.setAttribute("objectName",testL->objectName());
    root.appendChild(layout);

    QDomElement layoutGeometry = code.createElement("geometry");
    layoutGeometry.setAttribute("posX",testL->pos().x());
    layoutGeometry.setAttribute("posY",testL->pos().y());
    layoutGeometry.setAttribute("width",testL->width());
    layoutGeometry.setAttribute("height",testL->height());
    layout.appendChild(layoutGeometry);
    QDomElement layoutContent = code.createElement("content");

    for(int j=0; j < testL->layoutChildren()->size();j++)
    {
        QDomElement widget;
        if(dynamic_cast<Label*>(testL->layoutChildren()->values().at(j))!=NULL){
            Label* label = static_cast<Label*>(testL->layoutChildren()->values().at(j));
            widget = code.createElement("label");
            widget.setAttribute("objectName",label->objectName());
            widget.setAttribute("toolTip",label->toolTip());
            widget.setAttribute("enabled",label->isEnabled());
            QDomElement geometry = code.createElement("geometry");
            geometry.setAttribute("posX",label->pos().x());
            geometry.setAttribute("posY",label->pos().y());
            geometry.setAttribute("width",label->width());
            geometry.setAttribute("height",label->height());
            widget.appendChild(geometry);
            QDomElement text = code.createElement("content");
            text.setAttribute("text",label->text());
            widget.appendChild(text);
        } else if(dynamic_cast<PushButton*>(testL->layoutChildren()->values().at(j))!=NULL){
            PushButton* bt = static_cast<PushButton*>(testL->layoutChildren()->values().at(j));
            widget = code.createElement("pushButton");
            widget.setAttribute("objectName",bt->objectName());
            widget.setAttribute("toolTip",bt->toolTip());
            widget.setAttribute("enabled",bt->isEnabled());
            QDomElement geometry = code.createElement("geometry");
            geometry.setAttribute("posX",bt->pos().x());
            geometry.setAttribute("posY",bt->pos().y());
            geometry.setAttribute("width",bt->width());
            geometry.setAttribute("height",bt->height());
            widget.appendChild(geometry);
            QDomElement text = code.createElement("content");
            text.setAttribute("text",bt->text());
            widget.appendChild(text);
        } else if(dynamic_cast<CheckBox*>(testL->layoutChildren()->values().at(j))!=NULL){
            CheckBox* cb = static_cast<CheckBox*>(testL->layoutChildren()->values().at(j));
            widget = code.createElement("checkBox");
            widget.setAttribute("objectName",cb->objectName());
            widget.setAttribute("isChecked",cb->isChecked());
            widget.setAttribute("toolTip",cb->toolTip());
            widget.setAttribute("enabled",cb->isEnabled());
            QDomElement geometry = code.createElement("geometry");
            geometry.setAttribute("posX",cb->pos().x());
            geometry.setAttribute("posY",cb->pos().y());
            geometry.setAttribute("width",cb->width());
            geometry.setAttribute("height",cb->height());
            widget.appendChild(geometry);
            QDomElement text = code.createElement("content");
            text.setAttribute("text",cb->text());
            widget.appendChild(text);
        } else if(dynamic_cast<LayoutWidget*>(testL->layoutChildren()->values().at(j))!=NULL){
            LayoutWidget* lw = static_cast<LayoutWidget*>(testL->layoutChildren()->values().at(j));
            widget = layoutToXML(lw,layout);
        }

        layoutContent.appendChild(widget);
    }
    layout.appendChild(layoutContent);
    return layout;
}

void MainWindow::repaintTree()
{
    inspector->clear();

    QTreeWidgetItem* topLvlItem = new QTreeWidgetItem(0);
    topLvlItem->setText(0,"mainWindow");
    switch(windowFrame->layoutType){
    case WidgetType::Null:
        topLvlItem->setIcon(0,QIcon(":/new/icons/icons/nolayout.png"));
        break;
    case WidgetType::VBoxLayout:
        topLvlItem->setIcon(0,QIcon(":/new/icons/icons/vboxlayout.png"));
        break;
    case WidgetType::HBoxLayout:
        topLvlItem->setIcon(0,QIcon(":/new/icons/icons/hboxlayout.png"));
        break;
    default:
        break;
    }
    inspector->addTopLevelItem(topLvlItem);
    windowFrame->setTreeItem(topLvlItem);

    recourseRepaintTree(topLvlItem, windowFrame->layoutChildren());

    inspector->expandAll();
}

void MainWindow::recourseRepaintTree(QTreeWidgetItem* topLvlItem, QMap<QString, QWidget*>* widgetList)
{
    for(int j=0; j < widgetList->size();j++)
    {
        QTreeWidgetItem* newWI = new QTreeWidgetItem(0);
        //qDebug() << "_______________________________________________________";
        //qDebug() << widgetList->size();
        //qDebug() << widgetList->values().at(j)->objectName() << " - " <<  j << " from " << widgetList->size();
        widgetList->values().at(j);
        newWI->setText(0,widgetList->values().at(j)->objectName());

        if(dynamic_cast<Label*>(widgetList->values().at(j))!=NULL){
            static_cast<Label*>(widgetList->values().at(j))->setTreeItem(newWI);
            newWI->setText(1,"Label");
            newWI->setIcon(1,QIcon(":/new/icons/icons/label.png"));
        } else if(dynamic_cast<PushButton*>(widgetList->values().at(j))!=NULL){
            static_cast<PushButton*>(widgetList->values().at(j))->setTreeItem(newWI);
            newWI->setText(1,"PushButton");
            newWI->setIcon(1,QIcon(":/new/icons/icons/pushbutton.png"));
        } else if(dynamic_cast<CheckBox*>(widgetList->values().at(j))!=NULL){
            static_cast<CheckBox*>(widgetList->values().at(j))->setTreeItem(newWI);
            newWI->setText(1,"CheckBox");
            newWI->setIcon(1,QIcon(":/new/icons/icons/checkbox.png"));
        } else if(dynamic_cast<LayoutWidget*>(widgetList->values().at(j))!=NULL){
            LayoutWidget* lw = static_cast<LayoutWidget*>(widgetList->values().at(j));
            lw->setTreeItem(newWI);
            switch(lw->type){
            case WidgetType::Null:
                newWI->setText(1,"NoLayout");
                newWI->setIcon(1,QIcon(":/new/icons/icons/nolayout.png"));
                break;
            case WidgetType::VBoxLayout:
                newWI->setText(1,"VBoxLayout");
                newWI->setIcon(1,QIcon(":/new/icons/icons/vboxlayout.png"));
                break;
            case WidgetType::HBoxLayout:
                newWI->setText(1,"HBoxLayout");
                newWI->setIcon(1,QIcon(":/new/icons/icons/hboxlayout.png"));
                break;
            default:
                break;
            }
            recourseRepaintTree(newWI, lw->layoutChildren());
        }
        topLvlItem->addChild(newWI);
    }
}

void MainWindow::widgetSelected(QWidget* widget)
{
    setCurWidget(widget);
    reloadTable(widget);
}

void MainWindow::reloadTable(QWidget *widget)
{
    if(dynamic_cast<Label*>(widget)!=NULL)
    {
        Label* curLabel = static_cast<Label*>(widget);
        inspector->setCurrentItem(curLabel->getTreeItem());

        properties->clear();
        properties->setHorizontalHeaderLabels({"Параметр","Значение"});
        properties->setRowCount(7);
        QTableWidgetItem* objName = new QTableWidgetItem("objectName");
        QTableWidgetItem* objNameEdit = new QTableWidgetItem(curLabel->objectName());
        properties->setItem(0,0,objName);
        properties->setItem(0,1,objNameEdit);

        QTableWidgetItem* isEnabled = new QTableWidgetItem("isEnabled");
        QCheckBox* enabledCb = new QCheckBox("");
        enabledCb->setChecked(curLabel->isEnabled());
        properties->setItem(1,0,isEnabled);
        properties->setCellWidget(1,1,enabledCb);

        QTableWidgetItem* curPos = new QTableWidgetItem("curSize");
        QWidget* curPosEdit = new QWidget(properties);
        QHBoxLayout* curPosLayout = new QHBoxLayout;
        curPosEdit->setLayout(curPosLayout);
        curPosLayout->setMargin(0);
        curPosLayout->setSpacing(0);
        QSpinBox* spinX = new QSpinBox(curPosEdit);
        spinX->setMaximum(13333333);
        spinX->setMinimum(20);
        spinX->setValue(curLabel->x());
        QSpinBox* spinY = new QSpinBox(curPosEdit);
        spinY->setMaximum(13333333);
        spinY->setMinimum(20);
        spinY->setValue(curLabel->y());
        curPosLayout->addWidget(spinX);
        curPosLayout->addWidget(spinY);
        properties->setItem(2,0,curPos);
        properties->setCellWidget(2,1,curPosEdit);

        QTableWidgetItem* curSize = new QTableWidgetItem("curSize");
        QWidget* curSizeEdit = new QWidget(properties);
        QHBoxLayout* curSizeLayout = new QHBoxLayout;
        curSizeEdit->setLayout(curSizeLayout);
        curSizeLayout->setMargin(0);
        curSizeLayout->setSpacing(0);
        QSpinBox* spinWidth = new QSpinBox(curSizeEdit);
        spinWidth->setMaximum(13333333);
        spinWidth->setMinimum(20);
        spinWidth->setValue(curLabel->width());
        QSpinBox* spinHeight = new QSpinBox(curSizeEdit);
        spinHeight->setMaximum(13333333);
        spinHeight->setMinimum(20);
        spinHeight->setValue(curLabel->height());
        curSizeLayout->addWidget(spinWidth);
        curSizeLayout->addWidget(spinHeight);
        properties->setItem(3,0,curSize);
        properties->setCellWidget(3,1,curSizeEdit);

        QTableWidgetItem* isFixedSize = new QTableWidgetItem("isFixedSize");
        QCheckBox* fixedSize = new QCheckBox("");
        fixedSize->setChecked(curLabel->isEnabled()); //переделать на фиксед
        properties->setItem(4,0,isFixedSize);
        properties->setCellWidget(4,1,fixedSize);

        QTableWidgetItem* toolTip = new QTableWidgetItem("toolTip");
        QTableWidgetItem* toolTipEdit = new QTableWidgetItem(curLabel->toolTip());
        properties->setItem(5,0,toolTip);
        properties->setItem(5,1,toolTipEdit);

        QTableWidgetItem* text = new QTableWidgetItem("text");
        QTableWidgetItem* textEdit = new QTableWidgetItem(curLabel->text());
        properties->setItem(6,0,text);
        properties->setItem(6,1,textEdit);

    } else if(dynamic_cast<PushButton*>(widget)!=NULL)
    {
        PushButton* curButton = dynamic_cast<PushButton*>(widget);
        inspector->setCurrentItem(curButton->getTreeItem());

        properties->clear();
        properties->setHorizontalHeaderLabels({"Параметр","Значение"});
        properties->setRowCount(7);
        QTableWidgetItem* objName = new QTableWidgetItem("objectName");
        QTableWidgetItem* objNameEdit = new QTableWidgetItem(curButton->objectName());
        properties->setItem(0,0,objName);
        properties->setItem(0,1,objNameEdit);

        QTableWidgetItem* isEnabled = new QTableWidgetItem("isEnabled");
        QCheckBox* enabledCb = new QCheckBox("");
        enabledCb->setChecked(curButton->isEnabled());
        properties->setItem(1,0,isEnabled);
        properties->setCellWidget(1,1,enabledCb);

        QTableWidgetItem* curPos = new QTableWidgetItem("curSize");
        QWidget* curPosEdit = new QWidget(properties);
        QHBoxLayout* curPosLayout = new QHBoxLayout;
        curPosEdit->setLayout(curPosLayout);
        curPosLayout->setMargin(0);
        curPosLayout->setSpacing(0);
        QSpinBox* spinX = new QSpinBox(curPosEdit);
        spinX->setMaximum(13333333);
        spinX->setMinimum(20);
        spinX->setValue(curButton->x());
        QSpinBox* spinY = new QSpinBox(curPosEdit);
        spinY->setMaximum(13333333);
        spinY->setMinimum(20);
        spinY->setValue(curButton->y());
        curPosLayout->addWidget(spinX);
        curPosLayout->addWidget(spinY);
        properties->setItem(2,0,curPos);
        properties->setCellWidget(2,1,curPosEdit);

        QTableWidgetItem* curSize = new QTableWidgetItem("curSize");
        QWidget* curSizeEdit = new QWidget(properties);
        QHBoxLayout* curSizeLayout = new QHBoxLayout;
        curSizeEdit->setLayout(curSizeLayout);
        curSizeLayout->setMargin(0);
        curSizeLayout->setSpacing(0);
        QSpinBox* spinWidth = new QSpinBox(curSizeEdit);
        spinWidth->setMaximum(13333333);
        spinWidth->setMinimum(20);
        spinWidth->setValue(curButton->width());
        QSpinBox* spinHeight = new QSpinBox(curSizeEdit);
        spinHeight->setMaximum(13333333);
        spinHeight->setMinimum(20);
        spinHeight->setValue(curButton->height());
        curSizeLayout->addWidget(spinWidth);
        curSizeLayout->addWidget(spinHeight);
        properties->setItem(3,0,curSize);
        properties->setCellWidget(3,1,curSizeEdit);

        QTableWidgetItem* isFixedSize = new QTableWidgetItem("isFixedSize");
        QCheckBox* fixedSize = new QCheckBox("");
        fixedSize->setChecked(curButton->isEnabled()); //переделать на фиксед
        properties->setItem(4,0,isFixedSize);
        properties->setCellWidget(4,1,fixedSize);

        QTableWidgetItem* toolTip = new QTableWidgetItem("toolTip");
        QTableWidgetItem* toolTipEdit = new QTableWidgetItem(curButton->toolTip());
        properties->setItem(5,0,toolTip);
        properties->setItem(5,1,toolTipEdit);

        QTableWidgetItem* text = new QTableWidgetItem("text");
        QTableWidgetItem* textEdit = new QTableWidgetItem(curButton->text());
        properties->setItem(6,0,text);
        properties->setItem(6,1,textEdit);
    }else if(dynamic_cast<LayoutWidget*>(widget)!=NULL)
    {
        LayoutWidget* curLayoutWidget = dynamic_cast<LayoutWidget*>(widget);
        inspector->setCurrentItem(curLayoutWidget->getTreeItem());

        properties->clear();
        properties->setHorizontalHeaderLabels({"Параметр","Значение"});
        properties->setRowCount(4);
        QTableWidgetItem* objName = new QTableWidgetItem("objectName");
        QTableWidgetItem* objNameEdit = new QTableWidgetItem(curLayoutWidget->objectName());
        properties->setItem(0,0,objName);
        properties->setItem(0,1,objNameEdit);

        QTableWidgetItem* curPos = new QTableWidgetItem("curSize");
        QWidget* curPosEdit = new QWidget(properties);
        QHBoxLayout* curPosLayout = new QHBoxLayout;
        curPosEdit->setLayout(curPosLayout);
        curPosLayout->setMargin(0);
        curPosLayout->setSpacing(0);
        QSpinBox* spinX = new QSpinBox(curPosEdit);
        spinX->setMaximum(13333333);
        spinX->setMinimum(20);
        spinX->setValue(curLayoutWidget->x());
        QSpinBox* spinY = new QSpinBox(curPosEdit);
        spinY->setMaximum(13333333);
        spinY->setMinimum(20);
        spinY->setValue(curLayoutWidget->y());
        curPosLayout->addWidget(spinX);
        curPosLayout->addWidget(spinY);
        properties->setItem(1,0,curPos);
        properties->setCellWidget(1,1,curPosEdit);

        QTableWidgetItem* curSize = new QTableWidgetItem("curSize");
        QWidget* curSizeEdit = new QWidget(properties);
        QHBoxLayout* curSizeLayout = new QHBoxLayout;
        curSizeEdit->setLayout(curSizeLayout);
        curSizeLayout->setMargin(0);
        curSizeLayout->setSpacing(0);
        QSpinBox* spinWidth = new QSpinBox(curSizeEdit);
        spinWidth->setMaximum(13333333);
        spinWidth->setMinimum(20);
        spinWidth->setValue(curLayoutWidget->width());
        QSpinBox* spinHeight = new QSpinBox(curSizeEdit);
        spinHeight->setMaximum(13333333);
        spinHeight->setMinimum(20);
        spinHeight->setValue(curLayoutWidget->height());
        curSizeLayout->addWidget(spinWidth);
        curSizeLayout->addWidget(spinHeight);
        properties->setItem(2,0,curSize);
        properties->setCellWidget(2,1,curSizeEdit);

        QTableWidgetItem* isFixedSize = new QTableWidgetItem("isFixedSize");
        QCheckBox* fixedSize = new QCheckBox("");
        fixedSize->setChecked(curLayoutWidget->isEnabled()); //переделать на фиксед
        properties->setItem(3,0,isFixedSize);
        properties->setCellWidget(3,1,fixedSize);
    } else if(dynamic_cast<CheckBox*>(widget)!=NULL)
    {
        CheckBox* checkBox = static_cast<CheckBox*>(widget);
        inspector->setCurrentItem(checkBox->getTreeItem());

        properties->clear();
        properties->setHorizontalHeaderLabels({"Параметр","Значение"});
        properties->setRowCount(8);
        QTableWidgetItem* objName = new QTableWidgetItem("objectName");
        QTableWidgetItem* objNameEdit = new QTableWidgetItem(checkBox->objectName());
        properties->setItem(0,0,objName);
        properties->setItem(0,1,objNameEdit);

        QTableWidgetItem* isEnabled = new QTableWidgetItem("isEnabled");
        QCheckBox* enabledCb = new QCheckBox("");
        enabledCb->setChecked(checkBox->isEnabled());
        properties->setItem(1,0,isEnabled);
        properties->setCellWidget(1,1,enabledCb);

        QTableWidgetItem* curPos = new QTableWidgetItem("curSize");
        QWidget* curPosEdit = new QWidget(properties);
        QHBoxLayout* curPosLayout = new QHBoxLayout;
        curPosEdit->setLayout(curPosLayout);
        curPosLayout->setMargin(0);
        curPosLayout->setSpacing(0);
        QSpinBox* spinX = new QSpinBox(curPosEdit);
        spinX->setMaximum(13333333);
        spinX->setMinimum(20);
        spinX->setValue(checkBox->x());
        QSpinBox* spinY = new QSpinBox(curPosEdit);
        spinY->setMaximum(13333333);
        spinY->setMinimum(20);
        spinY->setValue(checkBox->y());
        curPosLayout->addWidget(spinX);
        curPosLayout->addWidget(spinY);
        properties->setItem(2,0,curPos);
        properties->setCellWidget(2,1,curPosEdit);

        QTableWidgetItem* curSize = new QTableWidgetItem("curSize");
        QWidget* curSizeEdit = new QWidget(properties);
        QHBoxLayout* curSizeLayout = new QHBoxLayout;
        curSizeEdit->setLayout(curSizeLayout);
        curSizeLayout->setMargin(0);
        curSizeLayout->setSpacing(0);
        QSpinBox* spinWidth = new QSpinBox(curSizeEdit);
        spinWidth->setMaximum(13333333);
        spinWidth->setMinimum(20);
        spinWidth->setValue(checkBox->width());
        QSpinBox* spinHeight = new QSpinBox(curSizeEdit);
        spinHeight->setMaximum(13333333);
        spinHeight->setMinimum(20);
        spinHeight->setValue(checkBox->height());
        curSizeLayout->addWidget(spinWidth);
        curSizeLayout->addWidget(spinHeight);
        properties->setItem(3,0,curSize);
        properties->setCellWidget(3,1,curSizeEdit);

        QTableWidgetItem* isFixedSize = new QTableWidgetItem("isFixedSize");
        QCheckBox* fixedSize = new QCheckBox("");
        fixedSize->setChecked(checkBox->isEnabled()); //переделать на фиксед
        properties->setItem(4,0,isFixedSize);
        properties->setCellWidget(4,1,fixedSize);

        QTableWidgetItem* toolTip = new QTableWidgetItem("toolTip");
        QTableWidgetItem* toolTipEdit = new QTableWidgetItem(checkBox->toolTip());
        properties->setItem(5,0,toolTip);
        properties->setItem(5,1,toolTipEdit);

        QTableWidgetItem* text = new QTableWidgetItem("text");
        QTableWidgetItem* textEdit = new QTableWidgetItem(checkBox->text());
        properties->setItem(6,0,text);
        properties->setItem(6,1,textEdit);

        QTableWidgetItem* isChecked = new QTableWidgetItem("isChecked");
        QCheckBox* cbIsChecked = new QCheckBox("");
        fixedSize->setChecked(checkBox->isChecked()); //переделать на фиксед
        properties->setItem(7,0,isChecked);
        properties->setCellWidget(7,1,cbIsChecked);
    }
    else if(dynamic_cast<FormWidget*>(widget)!=NULL)
    {
        FormWidget* form = static_cast<FormWidget*>(widget);
        inspector->setCurrentItem(form->getTreeItem());

        properties->clear();
        properties->setHorizontalHeaderLabels({"Параметр","Значение"});
        properties->setRowCount(2);
        QTableWidgetItem* objName = new QTableWidgetItem("objectName");
        QTableWidgetItem* objNameEdit = new QTableWidgetItem(form->objectName());
        properties->setItem(0,0,objName);
        properties->setItem(0,1,objNameEdit);

        QTableWidgetItem* curSize = new QTableWidgetItem("curSize");
        QWidget* curSizeEdit = new QWidget(properties);
        QHBoxLayout* curSizeLayout = new QHBoxLayout;
        curSizeEdit->setLayout(curSizeLayout);
        curSizeLayout->setMargin(0);
        curSizeLayout->setSpacing(0);
        QSpinBox* spinWidth = new QSpinBox(curSizeEdit);
        spinWidth->setMaximum(13333333);
        spinWidth->setMinimum(20);
        spinWidth->setValue(form->width());
        QSpinBox* spinHeight = new QSpinBox(curSizeEdit);
        spinHeight->setMaximum(13333333);
        spinHeight->setMinimum(20);
        spinHeight->setValue(form->height());
        curSizeLayout->addWidget(spinWidth);
        curSizeLayout->addWidget(spinHeight);
        properties->setItem(1,0,curSize);
        properties->setCellWidget(1,1,curSizeEdit);
    }
    else
        return;
    properties->setFocus();
}

void MainWindow::parceCode() //из кода на форму и в tree widget
{
    properties->clear();
    properties->setHorizontalHeaderLabels({"Параметр","Значение"});
    properties->setRowCount(0);

    windowFrame->clearChildren();

    code.setContent(text_code->toPlainText());

    curWidget = NULL;

    QDomElement root = code.firstChildElement();
    windowFrame->setFixedSize(root.attribute("width").toInt(),root.attribute("height").toInt());
    if(root.attribute("layoutType").contains("NULL"))
    {
        windowFrame->setNullLayout();
    } else if(root.attribute("layoutType").contains("VBoxLayout"))
    {
        windowFrame->setVBoxLayout();
    } else if(root.attribute("layoutType").contains("HBoxLayout"))
    {
        windowFrame->setHBoxLayout();
    }
    windowFrame->setSelected(false);
    parceNodes(windowFrame,root);

    rewriteCode();
    repaintTree();
    SendMSG("\n- Код из редактора применен.\n");
}

void MainWindow::parceNodes(QWidget* parentWidget, QDomElement root)
{
    for(int i =0;i<root.childNodes().size();i++)
    {
        QDomNode itemNode = root.childNodes().at(i);
        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            QString name = itemElem.tagName();
            if(itemElem.tagName().contains("layout"))
            {
                LayoutWidget* mainLayout;
                if (itemElem.attribute("layoutType").contains("VBoxLayout"))
                    mainLayout = addLayoutWhileParce(parentWidget, itemElem.attribute("objectName"), WidgetType::VBoxLayout,QPoint(0,0));
                else if (itemElem.attribute("layoutType").contains("HBoxLayout"))
                    mainLayout = addLayoutWhileParce(parentWidget, itemElem.attribute("objectName"), WidgetType::HBoxLayout,QPoint(0,0));
                else if(itemElem.attribute("layoutType").contains("NULL"))
                    mainLayout = addLayoutWhileParce(parentWidget, itemElem.attribute("objectName"), WidgetType::Null,QPoint(0,0));

                QDomElement geometryElem = itemElem.lastChildElement("geometry");
                int ex = geometryElem.attribute("posX").toInt();
                int ey = geometryElem.attribute("posY").toInt();
                int ewidth = geometryElem.attribute("width").toInt();
                int eheight = geometryElem.attribute("height").toInt();

                mainLayout->setGeometry(ex,ey,ewidth,eheight);
                mainLayout->setSelected(false);
                QDomElement contentElem = itemElem.lastChildElement("content");

                if(dynamic_cast<LayoutWidget*>(parentWidget)!=NULL)
                {
                    LayoutWidget* lw = static_cast<LayoutWidget*>(parentWidget);
                    //lw->layoutChildren()->insert(mainLayout->objectName(),mainLayout);
                    lw->insertToLayout(mainLayout->objectName(),mainLayout);
                } else if(dynamic_cast<FormWidget*>(parentWidget)!=NULL)
                {
                    windowFrame->layoutChildren()->insert(mainLayout->objectName(),mainLayout);
                }

                parceNodes(mainLayout, contentElem);
            }
            else if(itemElem.tagName().contains("label"))
            {
                QDomElement contentElem = itemElem.lastChildElement("content");
                Label* newLabel = addLabelWhileParce(parentWidget,itemElem.attribute("objectName"),QPoint(0,0));
                newLabel->setText(contentElem.attribute("text"));
                newLabel->setToolTip(itemElem.attribute("toolTip"));
                if(itemElem.attribute("enabled").toInt() == 1)
                    newLabel->setEnabled(true);
                else
                    newLabel->setEnabled(false);

                QDomElement geometryElem = itemElem.lastChildElement("geometry");
                int ex = geometryElem.attribute("posX").toInt();
                int ey = geometryElem.attribute("posY").toInt();
                int ewidth = geometryElem.attribute("width").toInt();
                int eheight = geometryElem.attribute("height").toInt();
                newLabel->setGeometry(ex,ey,ewidth,eheight);
                newLabel->setSelected(false);
            }
            else if(itemElem.tagName().contains("pushButton"))
            {
                QDomElement contentElem = itemElem.lastChildElement("content");
                PushButton* newPushButton= addPushButtonWhileParce(parentWidget,itemElem.attribute("objectName"),QPoint(0,0));
                newPushButton->setText(contentElem.attribute("text"));
                newPushButton->setToolTip(itemElem.attribute("toolTip"));
                if(itemElem.attribute("enabled").toInt() == 1)
                    newPushButton->setEnabled(true);
                else
                    newPushButton->setEnabled(false);

                QDomElement geometryElem = itemElem.firstChildElement("geometry");
                int ex = geometryElem.attribute("posX").toInt();
                int ey = geometryElem.attribute("posY").toInt();
                int ewidth = geometryElem.attribute("width").toInt();
                int eheight = geometryElem.attribute("height").toInt();
                newPushButton->setGeometry(ex,ey,ewidth,eheight);
                newPushButton->setSelected(false);
            }
            else if(itemElem.tagName().contains("checkBox"))
            {
                QDomElement contentElem = itemElem.lastChildElement("content");
                CheckBox* newCheckBox= addCheckBoxWhileParce(parentWidget,itemElem.attribute("objectName"),QPoint(0,0));
                newCheckBox->setText(contentElem.attribute("text"));
                newCheckBox->setToolTip(itemElem.attribute("toolTip"));
                if(itemElem.attribute("enabled").toInt() == 1)
                    newCheckBox->setEnabled(true);
                else
                    newCheckBox->setEnabled(false);

                if(itemElem.attribute("isChecked").toInt() == 1)
                    newCheckBox->setChecked(true);
                else
                    newCheckBox->setChecked(false);

                QDomElement geometryElem = itemElem.firstChildElement("geometry");
                int ex = geometryElem.attribute("posX").toInt();
                int ey = geometryElem.attribute("posY").toInt();
                int ewidth = geometryElem.attribute("width").toInt();
                int eheight = geometryElem.attribute("height").toInt();
                newCheckBox->setGeometry(ex,ey,ewidth,eheight);
                newCheckBox->setSelected(false);
            }
        }
    }
}

void MainWindow::cancelSelection(QWidget* widget)
{
    if(widget!=NULL)
    {
        if(dynamic_cast<Label*>(widget)!=NULL){
            Label* lb = static_cast<Label*>(widget);
            lb->setSelected(false);
        } else if(dynamic_cast<PushButton*>(widget)!=NULL){
            PushButton* bt = static_cast<PushButton*>(widget);
            bt->setSelected(false);
        } else if(dynamic_cast<CheckBox*>(widget)!=NULL){
            CheckBox* cb = static_cast<CheckBox*>(widget);
            cb->setSelected(false);
        } else if(dynamic_cast<FormWidget*>(widget)!=NULL){
            FormWidget* form = static_cast<FormWidget*>(widget);
            form->setSelected(false);
        } else if(dynamic_cast<LayoutWidget*>(widget)!=NULL){
            LayoutWidget* form = static_cast<LayoutWidget*>(widget);
            form->setSelected(false);
        }
    }
}

void MainWindow::newFile() //создать новый файл очистив все что было до этого
{
    properties->clear();
    properties->setHorizontalHeaderLabels({"Параметр","Значение"});
    properties->setRowCount(0);

    windowFrame->clearChildren();
    windowFrame->setNullLayout();
    setCurWidget(NULL);

    //clear window Frame

    SendMSG("- Новый файл. Все данные очищены.");
    rewriteCode();
    repaintTree();
    //полное обновление файлов
}

void MainWindow::openFile() //открыть ui файл
{
    filePath = QFileDialog::getOpenFileName(this,
                                QString::fromUtf8("Открыть файл"),
                                QDir::currentPath(),
                                "UI files (*.ui)");
    QFile file(filePath);
    QByteArray data;
    if (!file.open(QIODevice::ReadOnly))
        return;
    data = file.readAll();
    text_code->setPlainText(QString(data));

    file.close();

    SendMSG("- Открыт файл: " + filePath);

    parceCode();
}

void MainWindow::saveFile() //просто сохранить файл
{
    if(filePath==NULL)
        filePath = QFileDialog::getSaveFileName(this,
                                    QString::fromUtf8("Сохранить"),
                                    QDir::currentPath(),
                                    "UI files (*.ui)");
    QFile file(filePath);
    QTextStream out(&file);
    out.setCodec("UTF-8");
    if (!file.open(QIODevice::WriteOnly))
        return;
    out << text_code->toPlainText();

    SendMSG("- Файл сохранен.");

    file.close();
}

void MainWindow::saveFileAs() //сохранить ui файл как
{
    filePath = QFileDialog::getSaveFileName(this,
                                QString::fromUtf8("Сохранить как"),
                                QDir::currentPath(),
                                "UI files (*.ui)");
    QFile file(filePath);
    QTextStream out(&file);
    out.setCodec("UTF-8");
    if (!file.open(QIODevice::WriteOnly))
        return;
    out << text_code->toPlainText();

    SendMSG("- Файл сохранен как " + filePath);

    file.close();
}

void MainWindow::setFullScreen()
{
    if(ui->act_fullscreen->isChecked())
        this->showFullScreen();
    else
        this->showNormal();
}

void MainWindow::viewWidgets(bool visible)
{
    if(visible)
        ui->dock_widgets->show();
    else
        ui->dock_widgets->hide();
}

void MainWindow::viewInspector(bool visible)
{
    if(visible)
        ui->dock_inspector->show();
    else
        ui->dock_inspector->hide();
}

void MainWindow::viewProperties(bool visible)
{
    if(visible)
        ui->dock_prop->show();
    else
        ui->dock_prop->hide();
}

void MainWindow::setLightStyle(bool visible)
{
    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(240,240,240));
    qApp->setPalette(darkPalette);
    ui->act_darkstyle->setChecked(false);
    ui->act_lightstyle->setChecked(true);
}

void MainWindow::setDarkStyle(bool visible)
{
    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(53,53,53));
    qApp->setPalette(darkPalette);
    ui->act_darkstyle->setChecked(true);
    ui->act_lightstyle->setChecked(false);
}

void MainWindow::helpDialog()
{

}

void MainWindow::infoDialog()
{
    QDialog* idialog = new QDialog(this);
    idialog->setWindowTitle("О программе");

    QHBoxLayout* vlayout = new QHBoxLayout();
    QLabel* labelPixmap = new QLabel();
    labelPixmap->setPixmap(QPixmap(":/new/icons/icons/fullicon.png"));
    vlayout->addWidget(labelPixmap);

    QLabel* labelText = new QLabel();
    QString text = "Form Editor 0.0.1\n\nОснован на библиотеке Qt 5.6\n\nСобрано: 19 Мая 2016 в 18:31:11"
                   "\n\nСоздано в процессе дипломного\nпроектирвоания Игорем Мелькером,\nпод руководством Олега Калинина.";
    labelText->setText(text);
    vlayout->addWidget(labelText);

    idialog->setLayout(vlayout);
    idialog->setFixedSize(QSize(350,150));
    idialog->show();
}

void MainWindow::setCurWidget(QWidget *widget)
{
    if(curWidget==widget)
        return;
    if(curWidget!=NULL)
        cancelSelection(curWidget);
    curWidget = widget;
}

void MainWindow::addToFrame(QWidget* parentWidget, WidgetType type, QPoint pos)
{
    emit contentChanged();
    switch (type) {
    case WidgetType::Label:
        addLabel(parentWidget,"",pos);
        break;
    case WidgetType::PushButton:
        addPushButton(parentWidget,"",pos);
        break;
    case WidgetType::CheckBox:
        addCheckBox(parentWidget,"",pos);
        break;
    case WidgetType::VBoxLayout:
        addLayout(parentWidget,"",WidgetType::VBoxLayout,pos);
        break;
    case WidgetType::HBoxLayout:
        addLayout(parentWidget,"",WidgetType::HBoxLayout,pos);
        break;
    default:
        break;
    }
}

void MainWindow::dragToFrame(QWidget *parentWidget, WidgetType type, QPoint pos, QWidget *dragedWidget)
{
    emit contentChanged();
    dragedWidget->move(pos);
    qDebug()<< dragedWidget->objectName();
    qDebug() << windowFrame->layoutChildren()->contains(dragedWidget->objectName());

    moveWidgetTo(dragedWidget->parentWidget(),parentWidget,dragedWidget->objectName());
    rewriteCode();
    repaintTree();
}

void MainWindow::clearLayout(QLayout *layout)
{
    QLayoutItem * item;
    QLayout * sublayout;
    QWidget * widget;
    while ((item = layout->takeAt(0))) {
        if ((sublayout = item->layout()) != 0) {
            clearLayout(item->layout());
        }
        else if ((widget = item->widget()) != 0) {
            widget->hide();
            delete widget;
        }
        else {delete item;}
    }
}

void MainWindow::SendMSG(QString msg)
{
    QString text = ui->text_msg->toPlainText();
    text.append("\n" + msg);
    ui->text_msg->setPlainText(text);
}

void MainWindow::on_tree_inspector_itemClicked(QTreeWidgetItem *item, int /*column*/)
{
    QWidget* selectedWidget = getByObjectName(windowFrame->layoutChildren(),item->text(0));
    if(selectedWidget==NULL)
        return;

    if(dynamic_cast<Label*>(selectedWidget)!=NULL)
    {
        Label* curLabel = static_cast<Label*>(selectedWidget);
        if(!curLabel->isSelected())
            curLabel->setSelected(true);
        else
            reloadTable(curLabel);
    } else if(dynamic_cast<PushButton*>(selectedWidget)!=NULL)
    {
        PushButton* curPushButton = static_cast<PushButton*>(selectedWidget);
        if(!curPushButton->isSelected())
            curPushButton->setSelected(true);
        else
            reloadTable(curPushButton);
    } else if(dynamic_cast<CheckBox*>(selectedWidget)!=NULL)
    {
        CheckBox* curCheckBox = static_cast<CheckBox*>(selectedWidget);
        if(!curCheckBox->isSelected())
            curCheckBox->setSelected(true);
        else
            reloadTable(curCheckBox);
    } else if(dynamic_cast<LayoutWidget*>(selectedWidget)!=NULL)
    {
        LayoutWidget* curLayout = static_cast<LayoutWidget*>(selectedWidget);
        if(!curLayout->isSelected())
            curLayout->setSelected(true);
        else
            reloadTable(curLayout);
    } else if(item->text(0).contains(windowFrame->objectName()))
    {
        selectedWidget = windowFrame;
        if(!windowFrame->isSelected())
            windowFrame->setSelected(true);
        else
            reloadTable(windowFrame);
    } else
        return;

    setCurWidget(selectedWidget);
}

QWidget* MainWindow::getByObjectName(QMap<QString, QWidget*>* widgetList,QString name)
{
    QWidget* widg = NULL;
    if(windowFrame->objectName().endsWith(name))
        return windowFrame;

    for(int i = 0 ; i < widgetList->size();i++)
    {
        if (widgetList->values().at(i)->objectName().endsWith(name))
            widg = widgetList->values().at(i);
        else if (dynamic_cast<LayoutWidget*>(widgetList->values().at(i))!=NULL)
        {
            LayoutWidget* layoutWidget = static_cast<LayoutWidget*>(widgetList->values().at(i));
            widg = getByObjectName(layoutWidget->layoutChildren(),name);
        }
        if(widg!=NULL)
            return widg;
    }
    return NULL;
}

void MainWindow::removeByObjectName(QMap<QString, QWidget*>* widgetList,QString name)
{
    if(widgetList->contains(name))
    {
        widgetList->remove(name);
        return;
    }

    for(int i = 0 ; i < widgetList->size();i++)
    {
        if (dynamic_cast<LayoutWidget*>(widgetList->values().at(i))!=NULL)
        {
            LayoutWidget* layoutWidget = static_cast<LayoutWidget*>(widgetList->values().at(i));
            removeByObjectName(layoutWidget->layoutChildren(),name);
        }
    }
}

void MainWindow::moveWidgetTo(QWidget* from, QWidget* to, QString key)
{
    if(dynamic_cast<FormWidget*>(from)!=NULL && dynamic_cast<FormWidget*>(to)!=NULL)
        static_cast<FormWidget*>(to)->insertWidget(static_cast<FormWidget*>(from)->takeWidget(key));
    else if(dynamic_cast<LayoutWidget*>(from)!=NULL && dynamic_cast<LayoutWidget*>(to)!=NULL)
        static_cast<LayoutWidget*>(to)->insertWidget(static_cast<LayoutWidget*>(from)->takeWidget(key));
    else if(dynamic_cast<FormWidget*>(from)!=NULL && dynamic_cast<LayoutWidget*>(to)!=NULL)
        static_cast<LayoutWidget*>(to)->insertWidget(static_cast<FormWidget*>(from)->takeWidget(key));
    else if(dynamic_cast<LayoutWidget*>(from)!=NULL && dynamic_cast<FormWidget*>(to)!=NULL)
        static_cast<FormWidget*>(to)->insertWidget(static_cast<LayoutWidget*>(from)->takeWidget(key));
}

void MainWindow::copyWidget(){
    QClipboard* clip = QApplication::clipboard();
    //clip->setMimeData(pmd);
}

void MainWindow::cutWidget(){

}

void MainWindow::pasteWidget(){

}

void MainWindow::delWidget()
{
    if(curWidget==NULL || dynamic_cast<FormWidget*>(curWidget)!=NULL)
        return;

    properties->clear();
    properties->setHorizontalHeaderLabels({"Параметр","Значение"});
    properties->setRowCount(0);

    SendMSG("- - " + curWidget->objectName() + " - удален.");

    removeByObjectName(windowFrame->layoutChildren(),curWidget->objectName());

    if(dynamic_cast<Label*>(curWidget)!=NULL)
    {
        Label* curLabel = static_cast<Label*>(curWidget);
        curLabel->deleteLater();
    } else if(dynamic_cast<PushButton*>(curWidget)!=NULL)
    {
        PushButton* curPushButton = static_cast<PushButton*>(curWidget);
        curPushButton->deleteLater();
    } else if(dynamic_cast<CheckBox*>(curWidget)!=NULL)
    {
        CheckBox* curCheckBox = static_cast<CheckBox*>(curWidget);
        curCheckBox->deleteLater();
    }
    else if(dynamic_cast<LayoutWidget*>(curWidget)!=NULL)
    {
        LayoutWidget* curLayout = static_cast<LayoutWidget*>(curWidget);
        curLayout->clearChildren();
        curLayout->deleteLater();
    }
    else
        return;
    curWidget =NULL;

    repaintTree();
    rewriteCode();
}
