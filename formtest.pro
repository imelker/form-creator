#-------------------------------------------------
#
# Project created by QtCreator 2016-03-17T15:33:31
#
#-------------------------------------------------

QT       += core gui xml
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = formtest
TEMPLATE = app
RC_ICONS = icons/fullicon.ico


SOURCES += main.cpp\
        mainwindow.cpp \
    formwidget.cpp \
    codeeditor.cpp \
    pushbutton.cpp \
    checkbox.cpp \
    label.cpp \
    listwidget.cpp \
    layoutwidget.cpp \
    xmlhighlighter.cpp

HEADERS  += mainwindow.h \
    formwidget.h \
    codeeditor.h \
    pushbutton.h \
    checkbox.h \
    label.h \
    listwidget.h \
    layoutwidget.h \
    xmlhighlighter.h \
    enums.h

FORMS    += mainwindow.ui

RESOURCES += \
    icons.qrc
