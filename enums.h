#ifndef ENUMS_H
#define ENUMS_H

#include <QDebug>
#include <QMouseEvent>
#include <QMimeData>
#include <QPainter>

enum class WidgetType{Null,MainWindow,VBoxLayout,HBoxLayout,HSpacer,VSpacer,Label,PushButton,CheckBox};
enum class ResizeType{Null,LeftTopResize,RightTopResize,MidTopResize,LeftBotResize,RightBotResize,MidBotResize,LeftMidResize,RightMidResize};

#endif // ENUMS_H
