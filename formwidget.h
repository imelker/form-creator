#ifndef FORMWIDGET_H
#define FORMWIDGET_H

#include <QFrame>
#include <QPainter>
#include <QStyleOptionFrame>
#include <QStyle>
#include <QMouseEvent>
#include <QList>
#include <QTreeWidgetItem>
#include <QDebug>
#include "enums.h"
#include "pushbutton.h"
#include "label.h"
#include "layoutwidget.h"
#include "checkbox.h"

class FormWidget : public QFrame
{
    Q_OBJECT

public:
    explicit FormWidget(QWidget *parent = 0);
    ~FormWidget();
    QMenu* contextMenu;

private:
    bool _selected;
    int mousePosX,mousePosY;
    QMap<QString, QWidget*>* children;
    QTreeWidgetItem* insepctorItem;
    QRect _lt,_rt,_mt,_lb,_rb,_mb,_lm,_rm;
    QRect tempRect;
    int resiseRectSize;

public:
    ResizeType resizeType;
    WidgetType type;
    WidgetType layoutType;

    bool isSelected(){return _selected;}
    bool hasChildren();
    bool clearChildren();
    void actiavateLayout(bool activate);
    QMap<QString, QWidget*>* layoutChildren(){return children;}
    void setSelected(bool value);
    void setTreeItem(QTreeWidgetItem* item);
    QWidget* takeWidget(QString key);
    QWidget* insertWidget(QWidget *widget);
    QTreeWidgetItem* getTreeItem(){return insepctorItem;}

signals:
    void formSelected();
    void clicked();
    void selected(QWidget*);
    void addingToForm(QWidget* parentWidget, WidgetType widgetType, QPoint pos  = QPoint());
    void draggingToForm(QWidget* parentWidget, WidgetType widgetType, QPoint pos, QWidget* dragedWidget);

public slots:
    void setVBoxLayout();
    void setHBoxLayout();
    void setNullLayout();

protected: //переопределение виртуальных классов
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
};

#endif // FORMWIDGET_H
