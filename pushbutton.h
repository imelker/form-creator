#ifndef PUSHBUTTON_H
#define PUSHBUTTON_H

#include <QStackedWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>
#include <QTreeWidget>
#include <QDebug>
#include <QAbstractItemModel>
#include <QMouseEvent>
#include "enums.h"
#include "formwidget.h"
#include "layoutwidget.h"


class PushButton : public QPushButton
{
    Q_OBJECT
    static int countID(const int flag, const int id) //flag: 0 - обычная работа, 1 вызов при delete
    {
        static QVector<int> _ids;
        if(flag == 0){
            if(_ids.isEmpty())
                _ids.append(_ids.size());
            else if (_ids.last()!=-1)
                _ids.append(_ids.size());

            for(int i = 0; i < _ids.size();i++)
            {
                if(_ids.at(i)==-1){
                    _ids.removeAt(i);
                    _ids.insert(i,i);
                    qDebug() << _ids.at(i)<< " - " << i;
                    return i;
                } else if (i==_ids.size()-1)
                {
                    return i;
                }
            }
        } else if (1)
        {
            _ids.removeAt(id);
            _ids.insert(id,-1);
            return -1;
        }
    }
public:
    explicit PushButton(QWidget *parent=0);
    ~PushButton();

    QMenu* contextMenu;

private:
    QLineEdit* edit;
    WidgetType type;
    QTreeWidgetItem* insepctorItem;
    bool _selected;
    int thisID;

    bool drag;
    int mousePosX,mousePosY;
    ResizeType resizeType;
    QRect _lt,_rt,_mt,_lb,_rb,_mb,_lm,_rm;
    QRect tempRect;
    int resiseRectSize;
    void resizeWidget(QMouseEvent *event);
    void setFixedS(bool fix);

public:

    bool isSelected(){return _selected;}
    //сеттеры и геттеры
    void setSelected(bool value);
    void setTreeItem(QTreeWidgetItem* item);
    QTreeWidgetItem* getTreeItem(){return insepctorItem;}

signals:
    void selected(QWidget*);
    void textChanged(QWidget*);

private slots:
    void changeText();

protected: //переопределение виртуальных методов
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *event);
};

#endif // PUSHBUTTON_H
