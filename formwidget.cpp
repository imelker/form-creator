#include "formwidget.h"

#include <QDrag>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QDataStream>
#include <QPixmap>
#include <QMenu>
#include <QLayout>
#include <QFormLayout>

FormWidget::FormWidget(QWidget *parent) : QFrame(parent)
{
    setObjectName("mainWindow");

    //настройки Фрейма
    type = WidgetType::MainWindow;
    _selected = false;
    resiseRectSize = 5;
    setFixedSize(300,200);
    resizeType = ResizeType::Null;
    setAcceptDrops(true);
    contextMenu = new QMenu(this);

    children = new QMap<QString, QWidget*>();
    layoutType = WidgetType::Null;
}

FormWidget::~FormWidget()
{

}

void FormWidget::setVBoxLayout()
{
    delete this->layout();
    QVBoxLayout* layout = new QVBoxLayout(this);
    this->setLayout(layout);
    layoutType = WidgetType::VBoxLayout;
    actiavateLayout(true);
}

void FormWidget::setHBoxLayout()
{
    delete this->layout();
    QHBoxLayout* layout = new QHBoxLayout(this);
    this->setLayout(layout);
    layoutType = WidgetType::HBoxLayout;
    actiavateLayout(true);
}

void FormWidget::setNullLayout()
{
    delete this->layout();
    QFormLayout* layout = new QFormLayout(this);
    this->setLayout(layout);
    layoutType = WidgetType::Null;
    actiavateLayout(true);
}

void FormWidget::actiavateLayout(bool activate)
{
    if(activate)
    {
        for(int i = 0 ; i < children->size();i++)
        {
            children->values().at(i)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
            this->layout()->addWidget(children->values().at(i));
        }
        this->layout()->setMargin(3);
        this->layout()->setSpacing(3);
        this->layout()->setAlignment(Qt::AlignCenter);
    } else
    {
        delete this->layout();
        for(int i = 0 ; i < children->size();i++)
        {
            children->values().at(i)->setParent(this);
        }
    }
}

bool FormWidget::hasChildren()
{
    if(children->size()!=0)
        return true;
    else
        return false;
}

void FormWidget::setSelected(bool value)
{
    if(value)
        _selected = true;
    else
        _selected = false;

    this->repaint();
}

void FormWidget::setTreeItem(QTreeWidgetItem *item)
{
    if(item!=NULL)
        insepctorItem = item;
}

QWidget* FormWidget::takeWidget(QString key)
{
    QWidget* widget;
    if(children->contains(key))
        widget = children->take(key);
    return widget;
}

bool FormWidget::clearChildren()
{
    for(int i=0;i<children->size();i++){
        if(dynamic_cast<Label*>(children->values().at(i))!=NULL)
        {
            Label* curLabel = static_cast<Label*>(children->values().at(i));
            delete curLabel;
        } else if(dynamic_cast<PushButton*>(children->values().at(i))!=NULL)
        {
            PushButton* curPushButton = static_cast<PushButton*>(children->values().at(i));
            delete curPushButton;
        } else if(dynamic_cast<CheckBox*>(children->values().at(i))!=NULL)
        {
            CheckBox* curCheckBox = static_cast<CheckBox*>(children->values().at(i));
            delete curCheckBox;
        } else if(dynamic_cast<LayoutWidget*>(children->values().at(i))!=NULL)
        {
            LayoutWidget* curLayout = static_cast<LayoutWidget*>(children->values().at(i));
            curLayout->clearChildren();
            delete curLayout;
        }
    }
    children->clear();
    return true;
}

QWidget *FormWidget::insertWidget(QWidget* widget)
{
    if(layoutType!=WidgetType::Null){
        widget->setParent(this);
        widget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
        children->insert(widget->objectName(),widget);
        this->layout()->addWidget(widget);
        widget->setVisible(true);
    } else
    {
        widget->setParent(this);
        children->insert(widget->objectName(),widget);
        widget->setVisible(true);
    }
}

void FormWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QStyleOptionFrame opt;
    initStyleOption(&opt);
    this->style()->drawControl(QStyle::CE_ShapedFrame, &opt, &painter, this);


    painter.save();

    painter.setBrush(QColor(240,240,240)); //ставим цвет кисти
    QRect thisSize;
    thisSize.setRect(rect().x(),rect().y(),rect().width()-1,rect().height()-1);
    painter.drawRect(thisSize);

    _lt.setRect(thisSize.x(),
                thisSize.y(),
                resiseRectSize,
                resiseRectSize);
    _lb.setRect(thisSize.x(),
                thisSize.y() + thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _rt.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y(),
                resiseRectSize,
                resiseRectSize);
    _rb.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y()+thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _mt.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                thisSize.y(),
                resiseRectSize,
                resiseRectSize);
    _mb.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                thisSize.y()+thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _rm.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                resiseRectSize,
                resiseRectSize);

    _lm.setRect(thisSize.x(),
                thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                resiseRectSize,
                resiseRectSize);

    if(_selected)
    {
        painter.setBrush(Qt::green);
        painter.drawRect(_lt);
        painter.drawRect(_lb);
        painter.drawRect(_rt);
        painter.drawRect(_rb);
        painter.drawRect(_mt);
        painter.drawRect(_rm);
        painter.drawRect(_lm);
        painter.drawRect(_mb);
    }
    painter.restore();

}

void FormWidget::mousePressEvent(QMouseEvent *event)
{
    if(geometry().contains(event->localPos().toPoint()) && !_selected)
    {
        this->setSelected(true);
        emit selected(this);
    }

    if( event->button() == Qt::RightButton)
    {
        contextMenu->exec(event->globalPos());
        return;
    }

    mousePosX = event->x();
    mousePosY = event->y();

    if (_selected){
        tempRect = rect();
        if(_rt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::RightTopResize;
        }
        else if(_lb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::LeftBotResize;
        }
        else if(_rb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeFDiagCursor);
            resizeType = ResizeType::RightBotResize;
        }
        else if(_mb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::MidBotResize;
        }
        else if(_rm.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::RightMidResize;
        }
    }
    emit clicked();
}

void FormWidget::mouseMoveEvent(QMouseEvent *event)
{
    int newWidth = tempRect.width() + (event->pos().x() - mousePosX);
    int newHeight = tempRect.height()  + (event->pos().y() - mousePosY);
    if(newWidth<20)
        newWidth = 20;
    if(newHeight<20)
        newHeight = 20;

    if(resizeType==ResizeType::RightBotResize){ //br
        setFixedSize(newWidth,newHeight);
    }
    else if ((resizeType==ResizeType::MidBotResize || resizeType==ResizeType::LeftBotResize)){ //mb
        setFixedSize(tempRect.width(),newHeight);
    }
    else if ((resizeType==ResizeType::RightMidResize || resizeType==ResizeType::RightTopResize)){ //rm
        setFixedSize(newWidth, tempRect.height());
    }

}

void FormWidget::mouseReleaseEvent(QMouseEvent *event)
{
    resizeType = ResizeType::Null;
    setCursor(Qt::ArrowCursor);
}

void FormWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("label")
            || event->mimeData()->hasFormat("vlayout")
            || event->mimeData()->hasFormat("hlayout")
            || event->mimeData()->hasFormat("button")
            || event->mimeData()->hasFormat("checkbox")){
        event->accept();
    }
    else
        event->ignore();
}

void FormWidget::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasFormat("label")
            || event->mimeData()->hasFormat("vlayout")
            || event->mimeData()->hasFormat("hlayout")
            || event->mimeData()->hasFormat("button")
            || event->mimeData()->hasFormat("checkbox")) {

        WidgetType type = WidgetType::Null;
        QPoint newPos = QPoint(event->pos().x(), event->pos().y());
        if (event->mimeData()->hasFormat("label")) {
            type = WidgetType::Label;
            if(dynamic_cast<Label*>(event->source())!=NULL)
            {
                Label* pushButton = static_cast<Label*>(event->source());
                event->setDropAction(Qt::MoveAction);
                event->accept();

                QByteArray pieceData = event->mimeData()->data("label");
                QDataStream dataStream(&pieceData, QIODevice::ReadOnly);
                QPoint hotSpot;
                dataStream >> hotSpot;

                newPos = event->pos() - hotSpot;

                emit draggingToForm(this, type, newPos, pushButton);
                return;
            }
        } else if (event->mimeData()->hasFormat("button")) {
            type = WidgetType::PushButton;
            if(dynamic_cast<PushButton*>(event->source())!=NULL)
            {
                PushButton* pushButton = static_cast<PushButton*>(event->source());
                event->setDropAction(Qt::MoveAction);
                event->accept();

                QByteArray pieceData = event->mimeData()->data("button");
                QDataStream dataStream(&pieceData, QIODevice::ReadOnly);
                QPoint hotSpot;
                dataStream >> hotSpot;

                newPos = event->pos() - hotSpot;

                emit draggingToForm(this, type, newPos, pushButton);
                return;
            }
        } else if (event->mimeData()->hasFormat("checkbox")) {
            type = WidgetType::CheckBox;
            if(dynamic_cast<CheckBox*>(event->source())!=NULL)
            {
                CheckBox* pushButton = static_cast<CheckBox*>(event->source());
                event->setDropAction(Qt::MoveAction);
                event->accept();

                QByteArray pieceData = event->mimeData()->data("checkbox");
                QDataStream dataStream(&pieceData, QIODevice::ReadOnly);
                QPoint hotSpot;
                dataStream >> hotSpot;

                newPos = event->pos() - hotSpot;

                emit draggingToForm(this, type, newPos, pushButton);
                return;
            }
        } else if (event->mimeData()->hasFormat("vlayout")) {
            type = WidgetType::VBoxLayout;
            if(dynamic_cast<LayoutWidget*>(event->source())!=NULL)
            {
                LayoutWidget* pushButton = static_cast<LayoutWidget*>(event->source());
                event->setDropAction(Qt::MoveAction);
                event->accept();

                QByteArray pieceData = event->mimeData()->data("vlayout");
                QDataStream dataStream(&pieceData, QIODevice::ReadOnly);
                QPoint hotSpot;
                dataStream >> hotSpot;

                newPos = event->pos() - hotSpot;

                emit draggingToForm(this, type, newPos, pushButton);
                return;
            }
        } else if (event->mimeData()->hasFormat("hlayout")) {
            type = WidgetType::HBoxLayout;
            if(dynamic_cast<LayoutWidget*>(event->source())!=NULL)
            {
                LayoutWidget* pushButton = static_cast<LayoutWidget*>(event->source());
                event->setDropAction(Qt::MoveAction);
                event->accept();

                QByteArray pieceData = event->mimeData()->data("hlayout");
                QDataStream dataStream(&pieceData, QIODevice::ReadOnly);
                QPoint hotSpot;
                dataStream >> hotSpot;

                newPos = event->pos() - hotSpot;

                emit draggingToForm(this, type, newPos, pushButton);
                return;
            }
        }

        event->setDropAction(Qt::MoveAction);
        event->accept();

        emit addingToForm(this, type, newPos);
    } else {
        event->ignore();
    }
}
