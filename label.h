#ifndef LABEL_H
#define LABEL_H

#include <QStackedWidget>
#include <QPainter>
#include <QLineEdit>
#include <QLabel>
#include <QLayout>
#include <QFrame>
#include <QTreeWidgetItem>
#include <QAbstractItemModel>
#include "enums.h"

class Label : public QLabel
{
    Q_OBJECT
    static int countID(const int flag, const int id) //flag: 0 - обычная работа, 1 вызов при delete
    {
        static QVector<int> _ids;
        if(flag == 0){
            if(_ids.isEmpty())
                _ids.append(_ids.size());
            else if (_ids.last()!=-1)
                _ids.append(_ids.size());

            for(int i = 0; i < _ids.size();i++)
            {
                if(_ids.at(i)==-1){
                    _ids.removeAt(i);
                    _ids.insert(i,i);
                    qDebug() << _ids.at(i)<< " - " << i;
                    return i;
                } else if (i==_ids.size()-1)
                {
                    return i;
                }
            }
        } else if (1)
        {
            _ids.removeAt(id);
            _ids.insert(id,-1);
            return -1;
        }
    }
public:
    explicit Label(QWidget *parent=0, Qt::WindowFlags f=0);
    ~Label();
    QMenu* contextMenu;

private:
    QLineEdit* edit;
    QTreeWidgetItem* insepctorItem;
    bool _selected;
    int thisID;
    WidgetType type;

    bool drag;
    int mousePosX,mousePosY;
    ResizeType resizeType;
    QRect _lt,_rt,_mt,_lb,_rb,_mb,_lm,_rm;
    QRect tempRect;
    int resiseRectSize;
    void resizeWidget(QMouseEvent *event);
    void setFixedS(bool fix);

public:
    bool isSelected(){return _selected;}

    void setSelected(bool value);
    void setTreeItem(QTreeWidgetItem* item);
    QTreeWidgetItem* getTreeItem(){return insepctorItem;}

signals:
    void selected(QWidget*);
    void textChanged(QWidget*);

private slots:
    void changeText();

protected: //переопределение виртуальных методов
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
};

#endif // LABEL_H
