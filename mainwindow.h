#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLayout>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QTableWidget>
#include <QMap>
#include <QStringList>
#include <QToolBar>
#include <QHeaderView>
#include <QDebug>
#include <QDockWidget>
#include <QtXml/QDomDocument>
#include "formwidget.h"
#include "codeeditor.h"
#include "enums.h"
#include "layoutwidget.h"
#include "pushbutton.h"
#include "checkbox.h"
#include "label.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    void initEditor();
    void initToolBar();
    void initMenu();

signals:
    void formSelected();
    void contentChanged();

private slots:
    void rewriteCode();
    QDomElement layoutToXML(LayoutWidget* testL,QDomElement root);

    void addToFrame(QWidget *parentWidget, WidgetType type, QPoint pos);
    void dragToFrame(QWidget *parentWidget, WidgetType type, QPoint pos, QWidget *dragedWidget);

    void widgetSelected(QWidget* widget);
    void reloadTable(QWidget* widget);
    void parceCode();

    void on_tree_inspector_itemClicked(QTreeWidgetItem *item, int);
    void delWidget();
    void copyWidget();
    void cutWidget();
    void pasteWidget();

    void repaintTree();
    void newFile();
    void openFile();
    void saveFile();
    void saveFileAs();
    void setFullScreen();
    void viewWidgets(bool visible);
    void viewInspector(bool visible);
    void viewProperties(bool visible);
    void setLightStyle(bool visible);
    void setDarkStyle(bool visible);
    void helpDialog();
    void infoDialog();

private:
    Ui::MainWindow *ui;
    FormWidget* windowFrame;
    CodeEditor* text_code;
    QTreeWidget* inspector;
    QTableWidget* properties;
    QWidget* curWidget;
    void setCurWidget(QWidget* widget);
    QString filePath;
    QDomDocument code;
    void SendMSG(QString msg); 
    void addLayout(QWidget *parentWidget, QString text, WidgetType type, QPoint pos);
    void addLabel(QWidget *parentWidget, QString text, QPoint pos);
    void addPushButton(QWidget *parentWidget, QString text, QPoint pos);
    void addCheckBox(QWidget *parentWidget, QString text, QPoint pos);
    void cancelSelection(QWidget *widget);

    LayoutWidget* addLayoutWhileParce(QWidget *parentWidget, QString text, WidgetType type, QPoint pos);
    Label* addLabelWhileParce(QWidget *parentWidget, QString text, QPoint pos);
    PushButton* addPushButtonWhileParce(QWidget *parentWidget, QString text, QPoint pos);
    CheckBox* addCheckBoxWhileParce(QWidget *parentWidget, QString text, QPoint pos);

    QWidget* getByObjectName(QMap<QString, QWidget *> *widgetList, QString name);
    void removeByObjectName(QMap<QString, QWidget*>* widgetList,QString name);
    void moveWidgetTo(QWidget *from, QWidget *to, QString key);

    void parceNodes(QWidget *parentWidget, QDomElement root);
    void recourseRepaintTree(QTreeWidgetItem *topLvlItem, QMap<QString, QWidget*>* widgetList);
    void clearLayout(QLayout *layout);
};

#endif // MAINWINDOW_H
