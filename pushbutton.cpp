#include "pushbutton.h"
#include <QMimeData>
#include <QApplication>
#include <QDesktopWidget>
#include <QDrag>
#include <QPoint>
#include <QPainter>
#include <QMenu>
#include <QList>
#include <QFont>

PushButton::PushButton(QWidget *parent) : QPushButton(parent)
{
    thisID = countID(0,-1);
    if(thisID!=0)
        setObjectName("pushButton_" + QString::number(thisID));
    else
        setObjectName("pushButton");

    //настройки
    _selected = false;
    type = WidgetType::PushButton;
    resizeType = ResizeType::Null;
    resiseRectSize = 5;
    drag = false;
    contextMenu = new QMenu(this);

    this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    this->setText("PushButton");

    //LineEdit и его настройки
    edit = new QLineEdit(this);
    edit->setAlignment(Qt::AlignCenter);
    edit->setGeometry(4,4,width()-8,height()-8);
    edit->setVisible(false);

    connect(edit,SIGNAL(editingFinished()),this,SLOT(changeText()));
}

PushButton::~PushButton()
{
    countID(1,thisID);
}

void PushButton::changeText()
{
    setText(edit->text());
    edit->setVisible(false);
    emit textChanged(this);
}

void PushButton::setSelected(bool value)
{
    if(value)
        _selected = true;
    else
    {
        _selected = false;
        edit->setVisible(false);
    }
    this->repaint();
}

void PushButton::setTreeItem(QTreeWidgetItem *item)
{
    if(item!=NULL)
        insepctorItem = item;
}

void PushButton::mousePressEvent(QMouseEvent *event)
{
    if(!_selected){
        this->setSelected(true);
        emit selected(this);
    }

    if( event->button() == Qt::RightButton)
    {
        contextMenu->exec(event->globalPos());
        return;
    }

    mousePosX = event->x();
    mousePosY = event->y();

    if (_selected){
        tempRect = geometry();
        if(_lt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeFDiagCursor);
            resizeType = ResizeType::LeftTopResize;
            edit->setVisible(false);
        }
        else if(_lm.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::LeftMidResize;
            edit->setVisible(false);
        }
        else if(_mt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::MidTopResize;
            edit->setVisible(false);
        }
        else if(_rt.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeBDiagCursor);
            resizeType = ResizeType::RightTopResize;
            edit->setVisible(false);
        }
        else if(_lb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeBDiagCursor);
            resizeType = ResizeType::LeftBotResize;
            edit->setVisible(false);
        }
        else if(_rb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeFDiagCursor);
            resizeType = ResizeType::RightBotResize;
            edit->setVisible(false);
        }
        else if(_mb.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeVerCursor);
            resizeType = ResizeType::MidBotResize;
            edit->setVisible(false);
        }
        else if(_rm.contains(event->localPos().toPoint()))
        {
            setCursor(Qt::SizeHorCursor);
            resizeType = ResizeType::RightMidResize;
            edit->setVisible(false);
        }
        else if(!edit->isVisible())
            drag = true;
    }
}

void PushButton::mouseReleaseEvent(QMouseEvent* /*event*/)
{
    resizeType = ResizeType::Null;
    drag = false;
    setCursor(Qt::ArrowCursor);
}

void PushButton::mouseMoveEvent(QMouseEvent *event)
{
    if(resizeType!=ResizeType::Null)
        resizeWidget(event);
    else if (drag)
    {
        this->setVisible(false);
        QByteArray itemData;
        QDataStream dataStream(&itemData, QIODevice::WriteOnly);
        QMimeData *mimeData = new QMimeData;
        QDrag *dragButton = new QDrag(this);
        this->setFixedS(false);

        QPixmap pixmap(this->size());
        _selected = false;
        this->render(&pixmap);
        _selected = true;
        QPoint hotSpot = event->pos();
        dataStream << hotSpot;

        mimeData->setData("button" , itemData);

        dragButton->setMimeData(mimeData);
        dragButton->setHotSpot(QPoint(event->pos().x(), event->pos().y()));
        dragButton->setPixmap(pixmap);

        if (dragButton->exec(Qt::MoveAction) == Qt::MoveAction)
            true;
        drag = false;
    }
}

void PushButton::resizeWidget(QMouseEvent *event)
{
    int newX=tempRect.x(), newY=tempRect.y();
    int newWidth = tempRect.width(), newHeight = tempRect.height();

    switch(resizeType){
    case ResizeType::LeftTopResize://+
        newWidth += -(event->pos().x() - mousePosX);
        newHeight += -(event->pos().y() - mousePosY);
        newX+= event->pos().x() - mousePosX;
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::MidTopResize: //+
        newHeight += -(event->pos().y() - mousePosY);
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::RightTopResize: //+
        newWidth += event->pos().x() - mousePosX;
        newHeight += -(event->pos().y() - mousePosY);
        newY+= event->pos().y() - mousePosY;
        break;
    case ResizeType::RightMidResize://+
        newWidth += event->pos().x() - mousePosX;
        break;
    case ResizeType::RightBotResize://+
        newWidth += event->pos().x() - mousePosX;
        newHeight += event->pos().y() - mousePosY;
        break;
    case ResizeType::MidBotResize: //+
        newHeight += event->pos().y() - mousePosY;
        break;
    case ResizeType::LeftBotResize: //+
        newWidth += -(event->pos().x() - mousePosX);
        newHeight += event->pos().y() - mousePosY;
        newX+= event->pos().x() - mousePosX;
        break;
    case ResizeType::LeftMidResize: //+
        newWidth += -(event->x() - mousePosX);
        newX+= event->pos().x() - mousePosX;
        break;
    case ResizeType::Null: //+
        newWidth += event->pos().x() - mousePosX;
        newHeight += event->pos().y() - mousePosY;
        break;
    }

    if(newWidth<20)
        newWidth = 20;
    if(newHeight<20)
        newHeight = 20;

    if(newX > (tempRect.x() + tempRect.width() - 20))
        newX =  tempRect.x() + tempRect.width() - 20;
    if(newY > ( tempRect.y() + tempRect.height() - 20))
        newY =   tempRect.y() + tempRect.height() - 20;

    setFixedSize(newWidth,newHeight);
    move(newX,newY);
    tempRect.setTopLeft(QPoint(newX, newY));
}

void PushButton::setFixedS(bool fix)
{
    if(fix)
    {
        this->setMaximumSize(size());
        this->setMinimumSize(size());
    }
    else
    {
        this->setMinimumSize(QSize(20,20));
        this->setMaximumSize(QSize(16777215,16777215));
    }
}

void PushButton::mouseDoubleClickEvent(QMouseEvent* /*e*/)
{
    //добавить то, что это правая кнопка мыши
    if(_selected)
    {
        edit->setGeometry(resiseRectSize,resiseRectSize,width()-2*resiseRectSize,height()-2*resiseRectSize);
        edit->setVisible(true);
        edit->setText(text());
    }
    repaint();
}

void PushButton::paintEvent(QPaintEvent *event)
{
    QPushButton::paintEvent(event);

    QPainter painter(this);
    painter.save();

    if(_selected)
    {
    QRect thisSize;
    thisSize.setRect(0,0,width()-1,height()-1);

    _lt.setRect(thisSize.x(),
                thisSize.y(),
                resiseRectSize,
                resiseRectSize);
    _lb.setRect(thisSize.x(),
                thisSize.y() + thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _rt.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y(),
                resiseRectSize,
                resiseRectSize);
    _rb.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y()+thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _mt.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                thisSize.y(),
                resiseRectSize,
                resiseRectSize);
    _mb.setRect(thisSize.x()+(thisSize.width()-resiseRectSize)/2,
                thisSize.y()+thisSize.height()-resiseRectSize,
                resiseRectSize,
                resiseRectSize);
    _rm.setRect(thisSize.x()+thisSize.width()-resiseRectSize,
                thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                resiseRectSize,
                resiseRectSize);

    _lm.setRect(thisSize.x(),
                thisSize.y()+(thisSize.height()-resiseRectSize)/2,
                resiseRectSize,
                resiseRectSize);

        QPen* pen = new QPen(Qt::black);
        pen->setWidth(1);
        painter.setPen(*pen);
        painter.setBrush(Qt::green);
        painter.drawRect(_lt);
        painter.drawRect(_lb);
        painter.drawRect(_rt);
        painter.drawRect(_rb);
        painter.drawRect(_mt);
        painter.drawRect(_rm);
        painter.drawRect(_lm);
        painter.drawRect(_mb);
    }
    painter.restore();
}
